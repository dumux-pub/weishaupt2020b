#!/bin/sh

### create a folder for the DUNE and DuMuX modules
### go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

# dune-common
# master # e73048ff0710a310ba4ccc81cf739d6dd501aa2f # 2020-07-17 12:20:18 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout master
git reset --hard e73048ff0710a310ba4ccc81cf739d6dd501aa2f
cd ..

# dune-geometry
# master # 073077786cf9ea6240653924af035ff07f12e391 # 2020-05-24 21:15:12 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-geometry
cd dune-geometry
git checkout master
git reset --hard 073077786cf9ea6240653924af035ff07f12e391
cd ..

# dune-grid
# master # 5094279f10eb0aa999fe8e9982e6578819f57dba # 2020-07-02 09:28:06 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout master
git reset --hard 5094279f10eb0aa999fe8e9982e6578819f57dba
cd ..

# dune-localfunctions
# master # 0d1685fec57f3874ec94a8245f9e51f31a1a073e # 2020-05-24 21:01:44 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout master
git reset --hard 0d1685fec57f3874ec94a8245f9e51f31a1a073e
cd ..

# dune-istl
# master # ff799f4f4bcf774aa20a851909934b4e82d1d2a8 # 2020-06-04 12:39:26 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout master
git reset --hard ff799f4f4bcf774aa20a851909934b4e82d1d2a8
cd ..

# dune-foamgrid
# master # bd68a6ff42136621c5321acabea17ac10d4e3997 # 2020-04-02 10:03:10 +0000 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git
cd dune-foamgrid
git checkout master
git reset --hard bd68a6ff42136621c5321acabea17ac10d4e3997
cd ..

# dune-subgrid
# master # 1bc0b375ffdcf39222416ecafea47db6f08e91f6 # 2020-05-06 15:13:58 +0000 # oliver.sander_at_tu-dresden.de
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git
cd dune-subgrid
git checkout master
git reset --hard 1bc0b375ffdcf39222416ecafea47db6f08e91f6
cd ..

# dumux
# master # 2b075424e4021400ecf443628e51d855dd10d650 # 2020-07-16 10:27:03 +0000 # Timo Koch
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout master
git reset --hard 2b075424e4021400ecf443628e51d855dd10d650
cd ..

# this module
git clone https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2020b.git

# run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all

# clean up
rm installWeishaupt2020b.sh
