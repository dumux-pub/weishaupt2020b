import os
import subprocess

def run(widthScale, pressureGradient, useSlurm):
    caseName = str(widthScale*100)

    problemName = caseName
    problemName += '_dp' if pressureGradient else '_shear'

    os.system('mkdir ' + problemName)

    wd = os.getcwd()

    xMin = str(-0.25e-3 * widthScale)
    yMin = str(-1e-4 * widthScale)
    xMax = str(0.25e-3 * widthScale)
    yMax = str(0.5e-3 * widthScale)

    command = 'srun ' if useSlurm else ''
    command += './../beta_2d '
    command += './../params.input '
    command += '-Grid.Cells "200 240" '
    command += '-Grid.LowerLeft "' + xMin + ' ' + yMin +'" '
    command += '-Grid.UpperRight "' + xMax + ' ' + yMax +'" '
    command += '-Grid.LeftBlockLowerLeft "' + str(-0.25e-3 * widthScale) + ' ' + str(-1e-4 * widthScale) +'" '
    command += '-Grid.LeftBlockUpperRight "' + str(-0.5e-4 * widthScale) + ' ' + str(0) +'" '
    command += '-Grid.RightBlockLowerLeft "' + str(0.5e-4 * widthScale) + ' ' + str(-1e-4 * widthScale) +'" '
    command += '-Grid.RightBlockUpperRight "' + str(0.25e-3 * widthScale) + ' ' + str(0) +'" '
    command += '-Problem.Name ' + problemName + ' '
    command += '-Problem.InletPressure ' + ('1e-6 ' if pressureGradient else '0 ')
    command += '-Problem.Slope ' + ('1 ' if not pressureGradient else '0 ')
    command += ' > ' + problemName + '.log' + ' 2> ' + problemName + '.err &'

    print (command)

    subprocess.Popen(command, cwd=wd + '/' + problemName, shell=True)


run(widthScale=1, pressureGradient=True, useSlurm=False)

#for i in [0.5, 1.0, 2.0, 4.0]:
    #print("Runnng", i*100)
    #run(widthScale=i, pressureGradient=True, useSlurm=True)
    #run(widthScale=i, pressureGradient=False, useSlurm=True)
