// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Setup to determine the beta factor in 2D.
 */

#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/common/typetraits/problem.hh>

#include <appl/beta_3d/evalbeta.hh>

#include "problem.hh"

namespace Dumux
{
namespace Properties {
// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::ChannelTest>
{
    using Scalar = GetPropType<TypeTag, Scalar>;
    using HostGrid = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<Scalar, 2>>;
    using type = Dune::SubGrid<2, HostGrid>;
};
} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::ChannelTest;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    using GridManager = GridManager<GetPropType<TypeTag, Properties::Grid>>;


    const auto elementSelector = [&](const auto& e)
    {
        using GlobalPosition = Dune::FieldVector<double, 2>;
        static const auto leftBlockUpperRight = getParam<GlobalPosition>("Grid.LeftBlockUpperRight");
        static const auto rightBlockLowerLeft = getParam<GlobalPosition>("Grid.RightBlockLowerLeft");
        static const auto rightBlockUpperRight = getParam<GlobalPosition>("Grid.RightBlockUpperRight");
        const auto& center = e.geometry().center();

        // channel
        if (center[1] > leftBlockUpperRight[1] || center[1] > rightBlockUpperRight[1])
            return true;

        // pore
        if (center[0] > leftBlockUpperRight[0] && center[0] < rightBlockLowerLeft[0])
            return true;

        return false;
    };

    GridManager gridManager; gridManager.init(elementSelector);

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    gridGeometry->update();

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(gridGeometry->numCellCenterDofs());
    x[GridGeometry::faceIdx()].resize(gridGeometry->numFaceDofs());

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // initialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.write(0);

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
    // using LinearSolver = Dumux::UMFPackBackend;
    using LinearSolver = Dumux::UzawaBiCGSTABBackend<GridGeometry>;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // set up two surfaces over which fluxes are calculated
    FluxOverSurface<GridVariables,
                    SolutionVector,
                    GetPropType<TypeTag, Properties::ModelTraits>,
                    GetPropType<TypeTag, Properties::LocalResidual>> flux(*gridVariables, x);
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    const std::vector<Scalar> throatPos0 = getParam<std::vector<Scalar>>("Problem.ThroatPos0", std::vector<Scalar>{});
    const std::vector<Scalar> throatPos1 = getParam<std::vector<Scalar>>("Problem.ThroatPos1", std::vector<Scalar>{});
    std::vector<std::string> surfaceNames;

    if (!throatPos0.empty())
    {
        if (throatPos0.size() != throatPos1.size())
            DUNE_THROW(Dune::InvalidStateException, "ThroatPos0 and ThroatPos1 must have the same number of entries");

        for (int i = 0; i < throatPos0.size(); i += 2)
        {
            const GlobalPosition leftPoint{throatPos0[i], throatPos1[i]};
            const GlobalPosition rightPoint{throatPos0[i+1], throatPos1[i+1]};
            auto surfaceName = "throat_" + std::to_string(i);
            flux.addSurface(surfaceName, leftPoint, rightPoint);
            surfaceNames.emplace_back(surfaceName);
        }
    }

    // solve the non-linear system with time step control
    nonLinearSolver.solve(x);

    // write vtk output
    vtkWriter.write(1.0);

    // calculate and print mass fluxes over the planes
    flux.calculateMassOrMoleFluxes();

    for (const auto& name : surfaceNames)
        std::cout << "mass flux at " << name <<  " is: " << flux.netFlux(name) << std::endl;

    // get gradients etc.
    using BoundaryTypes = typename ProblemTraits<Problem>::BoundaryTypes;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
    using VelocityGradients = StaggeredVelocityGradients<double, GridGeometry, BoundaryTypes, Indices>;
    const auto data = EvalBeta<VelocityGradients>::eval(*gridVariables, x);

    // create temporary stringstream with fixed formatting without affecting std::cout
    std::ostream tmp(std::cout.rdbuf());
    tmp << std::fixed << std::setprecision(15);

    tmp << "avg vx " << data.avgVx<< std::endl;
    tmp << "avg sumgrad " << data.avgSumGrad << std::endl;
    tmp << "avg beta " << data.avgBeta << std::endl;
    tmp << "avgSumGrad / avgVx " << data.avgSumGrad / data.avgVx << std::endl;

    using GlobalPosition = Dune::FieldVector<double, 2>;

    const auto poreDiameter = getParam<GlobalPosition>("Grid.RightBlockLowerLeft")[0] - getParam<GlobalPosition>("Grid.LeftBlockUpperRight")[0];
    tmp << "beta scaled by radius " << data.avgSumGrad / data.avgVx * 0.5*poreDiameter << std::endl;

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
