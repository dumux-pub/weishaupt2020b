// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Setup to determine the beta factor in 2D
 */
#ifndef DUMUX_CHANNEL_PROBLEM_HH
#define DUMUX_CHANNEL_PROBLEM_HH

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

namespace Dumux
{
template <class TypeTag>
class StructuredProblem;


namespace Properties {
// Create new type tags
namespace TTag {
struct ChannelTest { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ChannelTest>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};


// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::ChannelTest> { using type = Dumux::StructuredProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::ChannelTest> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::ChannelTest> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::ChannelTest> { static constexpr bool value = true; };
} // end namespace Properties

/*!
 * \brief  Setup to determine the beta factor in 2D
 */
template <class TypeTag>
class StructuredProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridView = typename GridGeometry::GridView;

    static constexpr auto dimWorld = GridView::dimensionworld;

    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    StructuredProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), eps_(1e-6)
    {
        useVelocityAtThroatBottom_ = getParam<bool>("Problem.UseVelocityAtThroatBottom", true);
        inletPressure_ = getParam<Scalar>("Problem.InletPressure", 1e-3);
        bottomPressure_ = getParam<Scalar>("Problem.ThroatPressure", 1e-3);
        bottomVelocity_ = getParam<std::array<Scalar, 2>>("Problem.BottomVelocity", std::array<Scalar, 2>{0, 0});

        const auto tmp = getParam<std::vector<Scalar>>("Problem.ThroatPos0", std::vector<Scalar>{});

        if (!tmp.empty())
        {
            throatPos0_.resize(tmp.size()/2);
            int counter = 0;
            for (int i = 0; i < tmp.size(); i += 2)
            {
                throatPos0_[counter][0] = tmp[i];
                throatPos0_[counter][1] = tmp[i+1];
                ++counter;
            }
        }

        for (const auto& x : throatPos0_)
            std::cout << x[0] << "  " << x[1] << std::endl;

        considerWallFriction_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.ConsiderWallFriction", false);
        height_ = considerWallFriction_ ? getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Height") : 1.0;
        extrusionFactor_ = considerWallFriction_ ? 2.0/3.0 * height_ : 1.0;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        static const bool useSymmetry = getParam<bool>("Problem.UseSymmetry", false);

        if (isInlet_(globalPos) || isOutlet_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
        }
        else if (useSymmetry && globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_)
        {
            values.setAllSymmetry();
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;


        if (isInlet_(globalPos))
            values[Indices::pressureIdx] = inletPressure_;

        static const Scalar slope = getParam<Scalar>("Problem.Slope");

        if (globalPos[1] >= 0.0)
            values[Indices::velocityXIdx] = slope * globalPos[1];

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return PrimaryVariables(0.0);
    }

    auto& throatPositionsX() const
    { return throatPos0_; }

    // \}

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume face.
     */
    using ParentType::source;
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const ElementFaceVariables& elemFaceVars,
                       const SubControlVolumeFace& scvf) const
    {
        auto source = NumEqVector(0.0);

        if (GridView::dimensionworld == 2 && considerWallFriction_)
        {
            static const Scalar factor = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PseudoWallFractionFactor", 8.0);
            source[scvf.directionIndex()] = this->pseudo3DWallFriction(scvf, elemVolVars, elemFaceVars, height_, factor);
        }

        return source;
    }

    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    { return extrusionFactor_; }

private:

    bool isInlet_(const GlobalPosition& globalPos) const
    {
        return (globalPos[1] > 0.0 && (globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_));
    }

    bool isOutlet_(const GlobalPosition& globalPos) const
    {
        return (globalPos[1] > 0.0 && (globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_));
    }

    bool isThroatBottom_(const GlobalPosition& globalPos) const
    {
        return (globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_);
    }

    bool isLowerWall_(const GlobalPosition& globalPos) const
    {
        return std::abs(globalPos[1]) < eps_ ;
    }

    Scalar eps_;
    bool useVelocityAtThroatBottom_;
    Scalar inletPressure_;
    std::array<Scalar, 2> bottomVelocity_;
    Scalar bottomPressure_;
    std::vector<std::array<Scalar, 2>> throatPos0_;
    bool considerWallFriction_;
    Scalar height_;
    Scalar extrusionFactor_;
};
} //end namespace

#endif
