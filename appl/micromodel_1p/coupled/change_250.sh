#!/bin/bash

#make backup
cp 250_micron_coupled_micromodel_2d.input 250_micron_coupled_micromodel_2d.bak

# change 250
sed -i "s/CellsOutletX = 50/CellsOutletX = 45/g" 250_micron_coupled_micromodel_2d.input
sed -i "s/CellsOutletX = 100/CellsOutletX = 90/g" 250_micron_coupled_micromodel_2d.input
sed -i "s/CellsOutletX = 200/CellsOutletX = 180/g" 250_micron_coupled_micromodel_2d.input
sed -i "s/LowerLeft = -2.625e-3 5.25e-3 # inlet length: 0.25e-2 m/LowerLeft = -2.5e-3 5e-3 # inlet length: 0.25e-2 m/g" 250_micron_coupled_micromodel_2d.input
sed -i "s/UpperRight = 0.0128785 6.25e-3 # outlet length: 0.25e-2 m, heigth channel: 1e-3 m/UpperRight = 0.0125 6e-3 # outlet length: 0.25e-2 m, heigth channel: 1e-3 m/g" 250_micron_coupled_micromodel_2d.input
sed -i "s/LowerLeft = 0 0/LowerLeft = 125e-6 0/g" 250_micron_coupled_micromodel_2d.input
sed -i "s/UpperRight = 0.01025 5.25e-3 # 20 x 10 block, block size 250e-6 m/UpperRight = 0.010125 0.005 # 20 x 10 block, block size 250e-6 m/g" 250_micron_coupled_micromodel_2d.input
