// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Auxiliary class used to select the elements for the final grid
 */
#ifndef DUMUX_MICROMODEL_ELEMENT_SELECTOR_HH
#define DUMUX_MICROMODEL_ELEMENT_SELECTOR_HH

#include <string>
#include <dune/common/fvector.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dune/geometry/multilineargeometry.hh>
#include <dumux/common/geometry/intersectspointgeometry.hh>

namespace Dumux
{

/*!
 * \brief Auxiliary class used to select the elements for the final grid
 */
template <class GridView>
class ElementSelector
{
    using Scalar = typename GridView::ctype;
    static constexpr int dim = GridView::dimension;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;
    using Rectangle = Dune::AxisAlignedCubeGeometry<Scalar, dim, dim>;
    using Triangle = Dune::MultiLinearGeometry<Scalar, dim, dim>;

public:
    ElementSelector(const std::string& modelParamGroup, const GlobalPosition& pnmLowerLeft, const GlobalPosition& pnmUpperRight)
    {
        const auto lowerLeft = getParamFromGroup<GlobalPosition>(modelParamGroup, "Grid.LowerLeft");
        const auto upperRight = getParamFromGroup<GlobalPosition>(modelParamGroup, "Grid.UpperRight");

        Rectangle porousMedium(GlobalPosition{lowerLeft[0], pnmLowerLeft[1]}, GlobalPosition{upperRight[0], pnmUpperRight[1]});
        Rectangle leftOfTriangle(GlobalPosition{lowerLeft[0], lowerLeft[1]}, GlobalPosition{pnmLowerLeft[0], pnmLowerLeft[1]});
        Rectangle rightOfTriangle(GlobalPosition{pnmUpperRight[0], lowerLeft[1]}, GlobalPosition{upperRight[0], pnmLowerLeft[1]});

        rectangles_.push_back(porousMedium);
        rectangles_.push_back(leftOfTriangle);
        rectangles_.push_back(rightOfTriangle);

        const bool triangularReservoir = getParamFromGroup<bool>(modelParamGroup, "Grid.TriangularReservoir", false);
        if (triangularReservoir)
        {
            // account for slope at bottom reservoir
            const Scalar reservoirY = getParamFromGroup<Scalar>(modelParamGroup, "Grid.TriangleHeight");
            const Scalar triangleTipWidth = getParamFromGroup<Scalar>(modelParamGroup, "Grid.TriangleTipWidth");

            std::vector<GlobalPosition> pointsLeftTriangle({GlobalPosition{pnmLowerLeft[0], lowerLeft[1]},
                                                            GlobalPosition{pnmLowerLeft[0] + 0.5*(pnmUpperRight[0]- pnmLowerLeft[0]) - 0.5*triangleTipWidth, lowerLeft[1]},
                                                            GlobalPosition{pnmLowerLeft[0], lowerLeft[1] + reservoirY}});

            std::vector<GlobalPosition> pointsRightTriangle({GlobalPosition{pnmLowerLeft[0] + 0.5*(pnmUpperRight[0]- pnmLowerLeft[0]) + 0.5*triangleTipWidth, lowerLeft[1]},
                                                             GlobalPosition{pnmUpperRight[0], lowerLeft[1]},
                                                             GlobalPosition{pnmUpperRight[0], lowerLeft[1] + reservoirY}});

            triangles_.emplace_back(Dune::GeometryTypes::simplex(2), pointsLeftTriangle);
            triangles_.emplace_back(Dune::GeometryTypes::simplex(2), pointsRightTriangle);
        }
    }

    //! Select all elements that are not cut-out by the recangles or triangles
    bool operator() (const Element& element) const
    {
        const auto center = element.geometry().center();

        for (auto&& triangle : triangles_)
        {
            if (intersectsPointGeometry(center, triangle))
                return false;
        }

        for (auto&& rectangle : rectangles_)
        {
            if (intersectsPointGeometry(center, rectangle))
                return false;
        }

        return true;
    }

private:
    std::vector<Rectangle> rectangles_;
    std::vector<Triangle> triangles_;
};

} //end namespace

#endif
