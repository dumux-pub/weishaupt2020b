// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the one-phase pore network model.
 */
#ifndef DUMUX_PNM1P_PROBLEM_HH
#define DUMUX_PNM1P_PROBLEM_HH


// base problem
#include <dumux/porousmediumflow/problem.hh>
// Pore network model
#include <dumux/porenetworkflow/1p/model.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/common/boundarytypes.hh>
#include <random>

#define ISOTHERMAL 1

namespace Dumux {

template<class GridGeometry, class Scalar>
class StructuredSpatialParams;

template<class Scalar>
struct SimpleTransmissibility;

template <class TypeTag>
class PNMOnePProblem;

namespace Properties
{
// Create new type tags
namespace TTag {
struct PNMOnePTypeTag { using InheritsFrom = std::tuple<PNMOneP>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PNMOnePTypeTag> { using type = Dumux::PNMOnePProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::PNMOnePTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PNMOnePTypeTag> { using type = Dune::FoamGrid<1, 2>; };

template<class TypeTag>
struct SinglePhaseTransmissibilityLaw<TypeTag, TTag::PNMOnePTypeTag>
{
    using type = SimpleTransmissibility<GetPropType<TypeTag, Properties::Scalar>>;
};


template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PNMOnePTypeTag>
{
    using type = StructuredSpatialParams<GetPropType<TypeTag, Properties::GridGeometry>,
                                         GetPropType<TypeTag, Properties::Scalar>>;
};

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::PNMOnePTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::PNMOnePTypeTag> { static constexpr bool value = true; };

}

template<class Scalar>
struct SimpleTransmissibility
{
    using Cache = EmptyCache;

    template<class Problem, class Element, class FVElementGeometry, class ElementVolumeVariables, class FluxVariablesCache>
    static Scalar singlePhaseTransmissibility(const Problem& problem,
                                              const Element& element,
                                              const FVElementGeometry& fvGeometry,
                                              const typename FVElementGeometry::SubControlVolumeFace& scvf,
                                              const ElementVolumeVariables& elemVolVars,
                                              const FluxVariablesCache& fluxVarsCache,
                                              const int phaseIdx)
    {
        return problem.spatialParams().transmissibility(element);
    }
};

/**
 * \brief The base class for spatial parameters for pore network models.
 */
template<class GridGeometry, class Scalar>
class StructuredSpatialParams : public PNMBaseSpatialParams<GridGeometry, Scalar, StructuredSpatialParams<GridGeometry, Scalar>>
{
    using ParentType = PNMBaseSpatialParams<GridGeometry, Scalar, StructuredSpatialParams<GridGeometry, Scalar>>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

public:

    StructuredSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        transmissibility_.resize(gridGeometry->gridView().size(0));
        static const Scalar upScaledThroatHydraulicResistance = 1.0 / getParamFromGroup<Scalar>("PNM", "Problem.TransmissibilityThroat");
        static const Scalar upScaledHalfPoreHydraulicResistance = 1.0 / getParamFromGroup<Scalar>("PNM", "Problem.TransmissibilityHalfPore");



        const bool useRandomDistribution = getParam<bool>("PNM.Grid.UseRandomDistribution", false);
        const Scalar mean = getParam<Scalar>("PNM.Grid.MeanError", 0.0);
        const Scalar stddev = getParam<Scalar>("PNM.Grid.StdDevError", 0.0);
        std::normal_distribution<> resistanceDist{mean, stddev};
        std::mt19937 generator;

        if (useRandomDistribution)
        {
            // allow to specify a seed to get reproducible results
            if (hasParam("PNM.Grid.ParameterRandomNumberSeed"))
            {
                const auto seed = getParam<unsigned int>("PNM.Grid.ParameterRandomNumberSeed");
                generator.seed(seed);
            }
            else
            {
                std::random_device rd;
                generator.seed(rd());
            }
        }


        for (const auto& element : elements(gridGeometry->gridView()))
        {
            const auto eIdx = gridGeometry->elementMapper().index(element);
            Scalar rHyd = upScaledThroatHydraulicResistance / 1e-3;

            for (int scvIdx = 0; scvIdx < 2; ++scvIdx)
            {
                // add the pore resistance if we are not a the coupling interface
                if (gridGeometry->poreLabel(gridGeometry->gridView().indexSet().subIndex(element, scvIdx, 1)) != 2)
                    rHyd += upScaledHalfPoreHydraulicResistance / 1e-3;
            }

            transmissibility_[eIdx] = 1.0 / rHyd;  // viscosity will be accounted for later
            if (useRandomDistribution)
                transmissibility_[eIdx] *= (1.0 + resistanceDist(generator)); // add some random error
        }
    }

    /*!
    * \brief Returns the transmissibility of a throat
    */
   Scalar transmissibility(const Element& element) const
   {
       const auto eIdx = this->gridGeometry().elementMapper().index(element);
       return transmissibility_[eIdx];
   }

   template<class GlobalPosition>
   Scalar porosityAtPos(const GlobalPosition& globalPos) const
   { return 1.0; }

private:
    std::vector<Scalar> transmissibility_;
};

template <class TypeTag>
class PNMOnePProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    template<class SpatialParams>
    PNMOnePProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                   std::shared_ptr<SpatialParams> spatialParams,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, spatialParams, "PNM"), couplingManager_(couplingManager)
    { }

    /*!
     * \name Simulation steering
     */
    // \{

    /*!
     * \name Problem parameters
     */
    // \{

#if ISOTHERMAL
    /*!
     * \brief Return the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}
#endif
     /*!
     * \name Boundary conditions
     */
    // \{
    //! Specifies which kind of boundary condition should be used for
    //! which equation for a finite volume on the boundary.
    BoundaryTypes boundaryTypes(const Element &element, const SubControlVolume &scv) const
    {
        BoundaryTypes bcTypes;
        if(couplingManager().isCoupledDof(CouplingManager::lowDimIdx, scv.dofIndex()))
            bcTypes.setAllCouplingNeumann();
        else // neuman for the remaining boundaries
            bcTypes.setAllNeumann();

        return bcTypes;
    }

        /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex (pore body) for which the condition is evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichlet(const Element &element,
                               const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);
        return values;
    }


    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The sub control volume
     *
     * For this method, the return parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    template<class ElementVolumeVariables>
    PrimaryVariables source(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolume &scv) const
    {
        PrimaryVariables values(0.0);

        const auto vIdx =  scv.dofIndex();

        if (couplingManager().isCoupledDof(CouplingManager::lowDimIdx, vIdx))
            values = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scv) / this->gridGeometry().poreVolume(vIdx);

        return values;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    template<class Vertex>
    PrimaryVariables initial(const Vertex& vertex) const
    { return PrimaryVariables(0.0); }

    // \}

    // \}

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    std::shared_ptr<CouplingManager> couplingManager_;



};
} //end namespace

#endif
