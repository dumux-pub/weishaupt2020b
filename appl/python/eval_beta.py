#### import the simple module from the paraview
from paraview.simple import *
from paraview.numpy_support import vtk_to_numpy

import numpy as np
import argparse
import os
from os.path import abspath

def main(args):

    filePath = abspath(args['file'].name)

    assert(args['dimension'] == 3 or args['dimension'] == 2)

    # create a new 'XML Unstructured Grid Reader'
    vtu = XMLUnstructuredGridReader(FileName=[filePath])
    vtu.CellArrayStatus = ['velocity_liq (m/s)']

    # create a new 'Cell Data to Point Data'
    cellDatatoPointData = CellDatatoPointData(Input=vtu)

    # create a new 'Gradient Of Unstructured DataSet'
    gradientOfUnstructuredDataSet = GradientOfUnstructuredDataSet(Input=cellDatatoPointData)
    gradientOfUnstructuredDataSet.ScalarArray = ['POINTS', 'velocity_liq (m/s)']

    # create a new 'Slice'
    slice = Slice(Input=gradientOfUnstructuredDataSet)
    slice.SliceType = 'Plane'
    slice.SliceOffsetValues = [0.0]
    if args['dimension'] == 3:
        slice.SliceType.Origin = [0.0, 0.0, args['slizePosZ']]
        slice.SliceType.Normal = [0.0, 0.0, 1.0]
    else:
        slice.SliceType.Origin = [0.0, args['slizePosZ'], 0.0]
        slice.SliceType.Normal = [0.0, 1.0, 0.0]

    gradients = vtk_to_numpy(servermanager.Fetch(slice).GetPointData().GetArray('Gradients'))
    velocity = vtk_to_numpy(servermanager.Fetch(slice).GetPointData().GetArray('velocity_liq (m/s)'))


    if args['dimension'] == 3:
        print("3D")
        sumGrad = gradients[:,2] + gradients[:,6]
    else:
        print("2D")
        sumGrad = gradients[:,1] + gradients[:,3]

    avgSumGrad = np.mean(sumGrad)
    avgU = np.mean(velocity[:,0])

    bounds = servermanager.Fetch(slice).GetBounds()
    print("slice", bounds)
    print("diameter", bounds[1]-bounds[0])

    print("avgSumGrad", avgSumGrad)
    print("avgU", avgU)
    print("avgSumGrad / avgU", avgSumGrad / avgU)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='pvpython ' + sys.argv[0], description='Calculate beta.')
    parser.add_argument('-f', '--file', type=argparse.FileType('r') , required=True, help="vtu file to be processed")
    parser.add_argument('-d', '--dimension', type=int , required=True, help="the dimension")
    parser.add_argument('-spz', '--slizePosZ', type=float, default=0.0, help="slize z position")
    args = vars(parser.parse_args())
    main(args)
