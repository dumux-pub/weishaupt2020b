import scipy.stats as stats
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np

from scipy.special import erf
from scipy.stats import lognorm

# explicit analytical form of the truncated lognormal distribution
# see https://arxiv.org/pdf/1708.06159.pdf
def trunc_norm_pdf(x, median, sigma, x_min, x_max):
    a3 = 0.5*np.sqrt(2)*(np.log(x_min) - np.log(median)) / sigma
    a8 = 0.5*np.sqrt(2)*(np.log(x_max) - np.log(median)) / sigma
    a = -np.sqrt(2)*np.exp(-0.5*1.0/(sigma**2) * (np.log(x/median))**2)
    return a / (np.sqrt(np.pi)*sigma * (erf(a3) - erf(a8)) * x)

#plot the analytical and empirical pdf of a given sample
def plot(mu, sigma, sample, plotLogNormal=True, plotTruncLogNormal=True, n_bins=200):
    n_samples = len(sample)
    x_min = min(sample)
    x_max = max(sample)
    t = np.linspace(min(sample), max(sample), n_samples)
    if plotLogNormal:
        plt.hist(np.random.lognormal(mu, sigma, n_samples), n_bins, normed=True, label='log normal sample')
        plt.plot(t, lognorm.pdf(t, s=sigma, scale=np.exp(mu)), label='log normal pdf', color='b')
    if plotTruncLogNormal:
        plt.hist(sample, bins=n_bins, normed=True, fc=(1, 0, 0, 0.4), label='trunc. log normal sample')
        plt.plot(t, lognorm.pdf(t, s=sigma, scale=np.exp(mu))/(lognorm.cdf(x_max, s=sigma, scale=np.exp(mu)) - lognorm.cdf(x_min, s=sigma, scale=np.exp(mu))), label='trunc. log normal pdf', color='r', ls='-.')
        plt.plot(t, trunc_norm_pdf(t, median=np.exp(mu), sigma=sigma, x_min=x_min, x_max=x_max), label='trunc. log normal pdf, explicit', ls='--')
    plt.legend(loc='upper right')
    plt.show()

# function that return a sample drawn from the truncated lognormal distribution
# see https://stackoverflow.com/questions/47933019/how-to-properly-sample-truncated-distributions
def trunc_lognorm(mu, sigma, n_samples, x_min, x_max, random_state=None):
    samples = np.zeros((0,))    # empty for now
    while samples.shape[0] < n_samples:
        if random_state is None:
            s = np.random.lognormal(mu, sigma, n_samples)
        else:
            s = random_state.lognormal(mu, sigma, n_samples)
        accepted = s[(s >= x_min) & (s <= x_max)]
        samples = np.concatenate((samples, accepted), axis=0)
    samples = samples[:n_samples] # we probably got more than needed, so discard extra ones
    return samples

# usage:
def example_simple():
    mu = 0.0
    sigma = 0.25

    sample = trunc_lognorm(mu=mu, sigma=sigma, n_samples=int(1e6), x_min=1e-6, x_max=1e6)
    print "mean is ", np.mean(sample), " (expected: " , np.exp(mu + (sigma**2)/2)  , ")"
    plot(mu, sigma, sample, n_bins=200)

# plot data provided in Joekar-Niasar et al. 2011
# see https://en.wikipedia.org/wiki/Log-normal_distribution for conversion of shape parameters
def example_pnm():
    x_min = 0.0408e-3
    x_max = 0.234e-3
    x_mean = 0.114e-3
    stdev = 0.169e-3
    variance = stdev**2

    mu = np.log(x_mean/np.sqrt(1+variance/(x_mean**2)))
    sigma = np.sqrt(np.log(1+variance/(x_mean**2)))

    sample = trunc_lognorm(mu=mu, sigma=sigma, n_samples=int(1e6), x_min=x_min, x_max=x_max, random_state=np.random.RandomState(55))
    print "mean is ", np.mean(sample), " (expected: " , np.exp(mu + (sigma**2)/2)  , ")"
    plot(mu, sigma, sample, n_bins=200, plotTruncLogNormal=False)

    plt.hist(sample, bins=200, weights=np.zeros_like(sample) + 1. / sample.size)
    plt.show()

# example_simple()
# example_pnm()
