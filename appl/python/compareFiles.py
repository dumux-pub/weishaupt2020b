import glob
import argparse
import sys
import get_l2_norm

def findCases(refCases):

    for ref in refCases:
        coupledDirs = glob.glob('./../coupled/*' + ref)
        slipCase = next(x for x in coupledDirs if '_slip_' in x)
        noslipCase = next(x for x in coupledDirs if '_noslip_' in x)
        #print('reference is', ref)
        #print('slipCase is', slipCase)
        #print('noslipCase is', noslipCase)
        return {'ref':ref, 'slip':slipCase, 'noslip':noslipCase}



if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='pvpython ' + sys.argv[0], description='Calculate beta.')
    parser.add_argument('-d', '--dir', type=str, nargs='+', required=True, help="vtu file to be processed")
    args = vars(parser.parse_args())

    for dir in args['dir']:
        if not dir.endswith('/'):
            dir += '/'

        refDir = glob.glob(dir)
        casePaths = findCases([x.replace('ref_', '') for x in refDir])

        print(refDir[0])
        refFile = glob.glob(refDir[0] + '*0.vtu')[0]
        slipFile = glob.glob(casePaths['slip'] + '*0.vtu')[0]
        noslipFile = glob.glob(casePaths['noslip'] + '*0.vtu')[0]

        print('Case', refFile)

        print('**** Slip ****')
        print('Comparing', slipFile)
        errorsSlip = get_l2_norm.getNorm({'referenceFile':open(refFile), 'coupledFile':open(slipFile), 'clipZPosition':1e-8})
        print("error v:", errorsSlip['errV'])
        print("error p:", errorsSlip['errP'])

        print('**** No-Slip ****')
        print('Comparing', noslipFile)
        errorsNoSlip = get_l2_norm.getNorm({'referenceFile':open(refFile), 'coupledFile':open(noslipFile), 'clipZPosition':1e-8})
        print("error v:", errorsNoSlip['errV'])
        print("error p:", errorsNoSlip['errP'])
        ratioV = errorsNoSlip['errV']/errorsSlip['errV']
        ratioP = errorsNoSlip['errP']/errorsSlip['errP']
        print('eV_noslip/eV_slip', ratioV)
        print('eP_noslip/eP_slip', ratioP)
        print('LaTex:  {:.5e} & {:.5e} & {:.5e} & {:.5e}'.format(errorsNoSlip['errP'], errorsNoSlip['errV'], ratioP, ratioV))
