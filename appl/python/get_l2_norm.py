#### import the simple module from the paraview
from paraview.simple import *
from paraview.numpy_support import vtk_to_numpy

import numpy as np
import argparse
import os
from os.path import abspath

def getNorm(args):

    referenceFilePath = abspath(args['referenceFile'].name)
    coupledFilePath = abspath(args['coupledFile'].name)

    # create a new 'XML Unstructured Grid Reader'
    coupledVtu = XMLUnstructuredGridReader(FileName=[coupledFilePath])
    coupledVtu.CellArrayStatus = ['p', 'rho', 'velocity_liq (m/s)', 'process rank', 'coupledLowDimElement']

    # create a new 'XML Unstructured Grid Reader'
    referenceVtu = XMLUnstructuredGridReader(FileName=[referenceFilePath])
    referenceVtu.CellArrayStatus = ['p', 'rho', 'velocity_liq (m/s)', 'process rank']

    # create a new 'Clip'
    referenceClip = Clip(Input=referenceVtu)
    referenceClip.ClipType = 'Plane'

    # Properties modified on referenceClip
    referenceClip.Invert = 0
    referenceClip.Crinkleclip = 1

    # Properties modified on referenceClip.ClipType
    referenceClip.ClipType.Origin = [0.0005000000237487257, 0.0005000000237487257, args['clipZPosition']]
    referenceClip.ClipType.Normal = [0.0, 0.0, 1.0]

    v_ref =  vtk_to_numpy(servermanager.Fetch(referenceClip).GetCellData().GetArray('velocity_liq (m/s)'))
    v_coupl =  vtk_to_numpy(servermanager.Fetch(coupledVtu).GetCellData().GetArray('velocity_liq (m/s)'))
    assert(len(v_ref) == len(v_coupl))

    p_ref =  vtk_to_numpy(servermanager.Fetch(referenceClip).GetCellData().GetArray('p'))
    p_coupl =  vtk_to_numpy(servermanager.Fetch(coupledVtu).GetCellData().GetArray('p'))
    assert(len(p_ref) == len(p_coupl))

    error_v =  np.linalg.norm(v_coupl - v_ref) / np.linalg.norm(v_ref)
    error_v_x =  np.linalg.norm(v_coupl[:,0] - v_ref[:,0]) / np.linalg.norm(v_ref[:,0])
    error_v_y =  np.linalg.norm(v_coupl[:,1] - v_ref[:,1]) / np.linalg.norm(v_ref[:,1])
    error_v_z =  np.linalg.norm(v_coupl[:,2] - v_ref[:,2]) / np.linalg.norm(v_ref[:,2])

    print("errVx", error_v_x)
    print("errVy", error_v_y)
    print("errVz", error_v_z)

    print("len_ref", len(v_ref), "len_x", len(v_ref[:,0]))

    error_p =  np.linalg.norm(p_coupl - p_ref) / np.linalg.norm(p_ref)

    return {'errV':error_v, 'errP':error_p}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='pvpython ' + sys.argv[0], description='Calculate the l2 error.')
    parser.add_argument('-rf', '--referenceFile', type=argparse.FileType('r') , required=True, help="reference vtu file to be processed")
    parser.add_argument('-cf', '--coupledFile', type=argparse.FileType('r') , required=True, help="coupled vtu file to be processed")
    parser.add_argument('-czp', '--clipZPosition', type=float, default=0.0010001000000047498, help="clip z position")
    args = vars(parser.parse_args())
    errors = getNorm(args)
    print("error v:", errors['errV'])
    print("error p:", errors['errP'])
