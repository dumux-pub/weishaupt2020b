#### import the simple module from the paraview
from paraview.simple import *
from paraview.numpy_support import vtk_to_numpy

import numpy as np
import argparse
import os
from os.path import abspath

def main(args):

    filePath = abspath(args['file'].name)
    
    # create a new 'XML Unstructured Grid Reader'
    vtu = XMLUnstructuredGridReader(FileName=[filePath])
    vtu.CellArrayStatus = ['p', 'rho', 'velocity_liq (m/s)', 'process rank']
    
    # create a new 'Clip'
    freeFlow = Clip(Input=vtu)
    freeFlow.ClipType = 'Plane'

    # Properties modified on clip
    freeFlow.Invert = 0
    freeFlow.Crinkleclip = 1

    # Properties modified on clip.ClipType
    freeFlow.ClipType.Origin = [0.0005000000237487257, 0.0005000000237487257, args['clipZPosition']]
    freeFlow.ClipType.Normal = [0.0, 0.0, 1.0]

    # create a new 'Slice'
    outletSlice = Slice(Input=freeFlow)
    outletSlice.SliceType = 'Plane'
    outletSlice.SliceOffsetValues = [0.0]
    outletSlice.SliceType.Origin = [args['slizePosX'], 0.0, 0.0]
    outletSlice.SliceType.Normal = [1.0, 0.0, 0.0]

    velocityOutlet = vtk_to_numpy(servermanager.Fetch(outletSlice).GetCellData().GetArray('velocity_liq (m/s)'))   
    magVelOutlet = np.asarray([np.linalg.norm(x) for x in velocityOutlet])
    print('Max vel outlet', np.max(magVelOutlet))

    hydraulicDiameter = 0.0005
    maxReOutlet = np.max(magVelOutlet) * hydraulicDiameter / 1e-6
    meanReOutlet = np.mean(magVelOutlet) * hydraulicDiameter / 1e-6

    print('Re outlet (max / mean)', maxReOutlet, meanReOutlet)

    throatSlice = Slice(Input=vtu)
    throatSlice.SliceType = 'Plane'
    throatSlice.SliceOffsetValues = [0.0]
    throatSlice.SliceType.Origin = [0.0, 0.0, args['throatSlizePosZ']]
    throatSlice.SliceType.Normal = [0.0, 0.0, 1.0] 

    velocityThroat= vtk_to_numpy(servermanager.Fetch(throatSlice).GetCellData().GetArray('velocity_liq (m/s)'))
    magVelThroat = np.asarray([np.linalg.norm(x) for x in velocityThroat])
    print('Max vel throat', np.max(magVelThroat))

    hydraulicDiameter = 0.0001
    maxReThroat= np.max(magVelThroat) * hydraulicDiameter / 1e-6
    meanReThroat = np.mean(magVelThroat) * hydraulicDiameter / 1e-6

    print('Re throat (max / mean)', maxReThroat, meanReThroat)
    
    print('ReMaxFF/ReMaxThroat {:.5f}'.format(maxReOutlet/maxReThroat))
    print('ReMeanFF/ReMeanThroat {:.5f}'.format(meanReOutlet/meanReThroat))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='pvpython ' + sys.argv[0], description='Calculate beta.')
    parser.add_argument('-f', '--file', type=argparse.FileType('r') , required=True, help="vtu file to be processed")
    parser.add_argument('-spX', '--slizePosX', type=float, default=0.000249, help="slize x position")
    parser.add_argument('-tspZ', '--throatSlizePosZ', type=float, default=-0.0001875, help="throat slize z position")
    parser.add_argument('-czp', '--clipZPosition', type=float, default=1e-8, help="clip z position")
    args = vars(parser.parse_args())
    main(args)

