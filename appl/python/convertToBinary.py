import argparse
from os.path import abspath
from paraview.simple import *

parser = argparse.ArgumentParser(prog='pvpython ' + sys.argv[0], description='Compress file by converting to binary format.')
parser.add_argument('-f', '--file', type=argparse.FileType('r') , required=True, help="vtu file to be processed")
args = vars(parser.parse_args())

filePath = abspath(args['file'].name)

print('Reading', filePath)

# create a new 'XML Unstructured Grid Reader'
vtuFile = XMLUnstructuredGridReader(FileName=[filePath])
vtuFile.CellArrayStatus = ['p', 'rho', 'velocity_liq (m/s)', 'process rank']

# save data
print('Converting')
SaveData(filename=filePath, proxy=vtuFile, CompressorType='LZMA', CompressionLevel='9')
print('Done')
