// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 */
#ifndef DUMUX_EVAL_BETA
#define DUMUX_EVAL_BETA

#include <unordered_set>
#include <vector>
#include <iostream>
#include <numeric>
#include <type_traits>
#include <optional>

#include <dune/common/exceptions.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>
#include <dumux/common/parameters.hh>

namespace Dumux {

template<class VelocityGradients>
class EvalBeta
{
public:

    template<class GridVariables, class SolutionVector>
    static auto eval(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        using Scalar = typename GridVariables::Scalar;

        const auto& problem = gridVariables.curGridVolVars().problem();
        const auto& gridGeometry = problem.gridGeometry();

        using GridGeometry = std::decay_t<decltype(gridGeometry)>;
        using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
        using SubControlVolumeFace = typename GridGeometry::LocalView::SubControlVolumeFace;
        using BoundaryTypes = std::decay_t<decltype(problem.boundaryTypes(std::declval<Element>(), std::declval<SubControlVolumeFace>()))>;

        // get gradients etc.
        auto elemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
        auto elemFaceVars = localView(gridVariables.curGridFaceVars());

        const auto interfaceLowerElementIndices = getIndicesOfElementsBelowInterface_(gridGeometry);

        std::vector<Element> elements;
        for (auto eIdx : interfaceLowerElementIndices)
            elements.push_back(gridGeometry.boundingBoxTree().entitySet().entity(eIdx));

        std::cout << "found " << elements.size() << " elements" << std::endl;

        std::vector<Scalar> vX;
        vX.reserve(elements.size());

        std::vector<Scalar> dvXdZ;
        dvXdZ.reserve(elements.size());

        std::vector<Scalar> dvZdX;
        dvZdX.reserve(elements.size());

        static const auto verbose = Dumux::getParam<bool>("Problem.EvalBeta.Verbose", false);

        for (const auto& element : elements)
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);

            elemVolVars.bind(element, fvGeometry, sol);
            elemFaceVars.bind(element, fvGeometry, sol);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (auto && scvf : scvfs(fvGeometry))
            {
                static const auto interfacePosZ = Dumux::getParam<Scalar>("Problem.EvalBeta.InterfacePosZ", 0.0);
                if (std::abs(scvf.center()[2] - interfacePosZ) < 1e-8)
                {
                    const auto& neighbor = gridGeometry.boundingBoxTree().entitySet().entity(scvf.outsideScvIdx());
                    if (verbose)
                    {
                        std::cout << "At own element " << gridGeometry.elementMapper().index(element) << ", neighbor "
                                  << gridGeometry.elementMapper().index(neighbor)
                                  << ", scvf center " << scvf.center() << std::endl;
                    }

                    Scalar ownVx = 0.0;
                    for (auto && s : scvfs(fvGeometry))
                    {
                        if (s.directionIndex() == 0)
                            ownVx += 0.5*sol[GridGeometry::faceIdx()][s.dofIndex()];
                    }

                    auto neighborfvGeometry = localView(gridGeometry);
                    neighborfvGeometry.bind(neighbor);
                    Scalar neighborVx = 0.0;
                    for (auto && s : scvfs(neighborfvGeometry))
                    {
                        if (s.directionIndex() == 0)
                            neighborVx += 0.5*sol[GridGeometry::faceIdx()][s.dofIndex()];
                    }

                    vX.push_back(0.5*ownVx + 0.5*neighborVx);
                    if (verbose)
                        std::cout << "vX " << vX.back() << std::endl;

                    // get gradients
                    const auto& faceVars = elemFaceVars[scvf];
                    const std::size_t numSubFaces = scvf.pairData().size();

                    std::optional<BoundaryTypes> currentScvfBoundaryTypes;
                    if (scvf.boundary())
                        DUNE_THROW(Dune::InvalidStateException, "No boundary expected");

                    std::size_t counter = 0;
                    Scalar dvXdZLocal = 0;
                    Scalar dvZdXLocal = 0;

                    for (int localSubFaceIdx = 0; localSubFaceIdx < numSubFaces; ++localSubFaceIdx)
                    {
                        const auto eIdx = scvf.insideScvIdx();
                        // Get the face normal to the face the dof lives on. The staggered sub face conincides with half of this lateral face.
                        const auto& lateralScvf = fvGeometry.scvf(eIdx, scvf.pairData(localSubFaceIdx).localLateralFaceIdx);

                        // Create a boundaryTypes object (will be empty if not at a boundary).
                        std::optional<BoundaryTypes> lateralFaceBoundaryTypes;

                        // Check if there is face/element parallel to our face of interest where the dof lives on. If there is no parallel neighbor,
                        // we are on a boundary where we have to check for boundary conditions.
                        if (lateralScvf.boundary())
                        {
                            // Retrieve the boundary types that correspond to the center of the lateral scvf. As a convention, we always query
                            // the type of BCs at the center of the element's "actual" lateral scvf (not the face of the staggered control volume).
                            // The value of the BC will be evaluated at the center of the staggered face.
                            //     --------T######V                 || frontal face of staggered half-control-volume
                            //     |      ||      | current scvf    #  lateral staggered face of interest (may lie on a boundary)
                            //     |      ||      |                 x  dof position
                            //     |      ||      x~~~~> vel.Self   -- element boundaries
                            //     |      ||      |                 T  position at which the type of boundary conditions will be evaluated
                            //     |      ||      |                    (center of lateral scvf)
                            //     ----------------                 V  position at which the value of the boundary conditions will be evaluated
                            //                                         (center of the staggered lateral face)
                            lateralFaceBoundaryTypes.emplace(problem.boundaryTypes(element, lateralScvf));
                        }


                        if (lateralScvf.directionIndex() == 0)
                        {
                            ++counter;
                            dvXdZLocal += 0.5*VelocityGradients::velocityGradJI(problem,
                                                                                element,
                                                                                fvGeometry,
                                                                                scvf,
                                                                                faceVars,
                                                                                currentScvfBoundaryTypes,
                                                                                lateralFaceBoundaryTypes,
                                                                                localSubFaceIdx);

                            dvZdXLocal += 0.5*VelocityGradients::velocityGradIJ(problem,
                                                                                element,
                                                                                fvGeometry,
                                                                                scvf,
                                                                                faceVars,
                                                                                currentScvfBoundaryTypes,
                                                                                lateralFaceBoundaryTypes,
                                                                                localSubFaceIdx);
                        }
                    }
                    if ((counter !=2))
                        DUNE_THROW(Dune::InvalidStateException, "Wrong counter");

                    if (verbose)
                        std::cout << "dvXdZ " << dvXdZLocal << ", dvZdX " << dvZdXLocal << std::endl;

                    dvXdZ.push_back(dvXdZLocal);
                    dvZdX.push_back(dvZdXLocal);
                }
            }
        }

        if ((dvXdZ.size() != dvZdX.size()) || (vX.size() != dvXdZ.size()))
            DUNE_THROW(Dune::InvalidStateException, "Containers have the wrong size");

        std::vector<Scalar> sumGrad(dvXdZ.size(), 0.0);
        for (int i = 0; i < sumGrad.size(); ++i)
            sumGrad[i] = dvXdZ[i] + dvZdX[i];

        std::vector<Scalar> beta(sumGrad.size(), 0.0);
        for (int i = 0; i < beta.size(); ++i)
            beta[i] = sumGrad[i] / vX[i];

        struct Result
        {
            Scalar avgVx;
            Scalar avgSumGrad;
            Scalar avgBeta;
        };

        auto average = [](const auto& v)
        { return std::accumulate(v.begin(), v.end(), 0.0) / v.size(); };

        return Result{average(vX), average(sumGrad), average(beta)};
    }

private:
    template<class GridGeometry>
    static auto getIndicesOfElementsBelowInterface_(const GridGeometry& gridGeometry)
    {
        using GridView = typename GridGeometry::GridView;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        using Scalar = typename GridView::ctype;

        using Rectangle = Dune::AxisAlignedCubeGeometry<Scalar,
                                                        GridView::dimension-1,
                                                        GridView::dimensionworld>;

        const auto firstLayerInCavity = []()
        {
            if constexpr (GridView::dimensionworld == 3)
            {
                static const auto firstLayerBboxMin = getParam<GlobalPosition>("Problem.EvalBeta.FirstLayerBboxMin", GlobalPosition{-1.0, -1.0, -1e-8});
                static const auto firstLayerBboxMax = getParam<GlobalPosition>("Problem.EvalBeta.FirstLayerBboxMax", GlobalPosition{1.0, 1.0, -0.5e-8});
                return Rectangle(firstLayerBboxMin, firstLayerBboxMax);
            }
            else
            {
                static const auto firstLayerBboxMin = getParam<GlobalPosition>("Problem.EvalBeta.FirstLayerBboxMin", GlobalPosition{-1.0, -1e-8});
                static const auto firstLayerBboxMax = getParam<GlobalPosition>("Problem.EvalBeta.FirstLayerBboxMax", GlobalPosition{1.0, -0.5e-8});
                return Rectangle(firstLayerBboxMin, firstLayerBboxMax);
            }
        }();

        const auto interfaceIntersections = intersectingEntities(firstLayerInCavity, gridGeometry.boundingBoxTree());
        std::unordered_set<std::size_t> interfaceLowerElementIndices;
        for (auto&& is : interfaceIntersections)
            interfaceLowerElementIndices.insert(is.second());

        return interfaceLowerElementIndices;
    }
};
}

#endif // DUMUX_EVAL_BETA
