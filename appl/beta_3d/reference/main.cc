// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the 1d-3d embedded mixed-dimension model coupling two
 *        one-phase porous medium flow problems
 */


#include <config.h>

#include <iostream>
#include <unordered_set>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/geometry/axisalignedcubegeometry.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>

#include <dumux/porenetworkflow/common/pnmvtkoutputmodule.hh>
#include <dumux/io/grid/porenetworkgridcreator.hh>
#include <dumux/io/grid/networkextrusiongridcreator.hh>

#include "./../util.hh"

#if ONLYCREATEGRID
#include <dune/grid/io/file/gmshwriter.hh>
#else

#include <dumux/linear/ffpnmseqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/common/typetraits/problem.hh>
#include "stokesproblem.hh"
#include "./../evalbeta.hh"

namespace Dumux {
    namespace Properties {

        // Set the grid type
        template<class TypeTag>
        struct Grid<TypeTag, TTag::StokesTypeTag>
        {
            using type = typename NetworkExtrusionGridCreator<3>::Grid;
        };

    } // end namespace Properties
} // end namespace Dumux

#endif

template<class LowDimGridManager, class BulkGridGeometry, class GlobalPosition>
auto getCouplingPoreSurfaces(LowDimGridManager& lowDimGridManager, const BulkGridGeometry& bulkGridGeometry, const GlobalPosition& freeFlowLowerLeft, const double scaling)
{
    using Rectangle = Dune::AxisAlignedCubeGeometry<double,
                                                    BulkGridGeometry::GridView::dimension-1,
                                                    BulkGridGeometry::GridView::dimensionworld>;

    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();
    const auto& gridData = lowDimGridManager.getGridData();
    static const auto poreRadiusIdx = gridData->parameterIndex("PoreRadius");

    struct SurfaceData
    {
        GlobalPosition lowDimPos;
        std::array<GlobalPosition,4> corners;
    };

    std::vector<SurfaceData> result;

    for (const auto& vertex : vertices(lowDimGridView))
    {
        const auto& lowDimPos = vertex.geometry().center();

        if (lowDimPos[2] > freeFlowLowerLeft[2] - 1e-6)
        {
            const auto poreRadius = gridData->parameters(vertex)[poreRadiusIdx] * scaling;

            auto lowerLeft = lowDimPos - GlobalPosition(poreRadius);
            lowerLeft[2] = lowDimPos[2];
            auto upperRight = lowDimPos + GlobalPosition(poreRadius);
            upperRight[2] = lowDimPos[2];

            const auto interfaceIntersections = intersectingEntities(Rectangle(lowerLeft, upperRight), bulkGridGeometry.boundingBoxTree());

            if (interfaceIntersections.empty())
                continue;

            GlobalPosition bBoxMin = GlobalPosition(std::numeric_limits<double>::max());
            GlobalPosition bBoxMax = GlobalPosition(std::numeric_limits<double>::min());

            for (const auto intersection : interfaceIntersections)
            {
                const auto bulkElemIdx = intersection.second();
                const auto& bulkElement = bulkGridGeometry.boundingBoxTree().entitySet().entity(bulkElemIdx);

                const auto geometry = bulkElement.geometry();

                if ((geometry.center() - lowDimPos).two_norm() > poreRadius)
                    continue;

                for (int cornerIdx = 0; cornerIdx < 4; ++cornerIdx) // use only bottom corners
                {
                    const auto& corner = geometry.corner(cornerIdx);

                    for (int i = 0; i < 3; i++)
                    {
                        using std::min;
                        using std::max;
                        bBoxMin[i] = min(bBoxMin[i], corner[i]);
                        bBoxMax[i] = max(bBoxMax[i], corner[i]);
                    }
                }
            }

            Rectangle rectangle(bBoxMin, bBoxMax);
            std::array<GlobalPosition,4> corners;
            for (int i = 0; i < 4; ++i)
                corners[i] = rectangle.corner(i);

            result.push_back({lowDimPos, std::move(corners)});
        }
    }

    return result;
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    Dune::Timer timer;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    auto defaultParams = [](Dune::ParameterTree& tree) { };

    // parse command line arguments and input file
    Parameters::init(argc, argv, defaultParams, "");

    // try to create a grid (from the given grid file or the input file)
    using LowDimGridManager = Dumux::PoreNetworkGridCreator<3>;
    LowDimGridManager lowDimGridManager;
    lowDimGridManager.init("PNM"); // pass parameter group

    if (getParam<bool>("PNM.Grid.DeleteExcessCouplingThroats", true))
        deleteExcessCouplingThroats(lowDimGridManager.grid());

    // define free flow domain above network
    static const auto scaling = getParam<double>("Grid.Scaling", 1.0);
    using GlobalPosition = Dune::FieldVector<double, 3>;
    const auto freeFlowLowerLeft = getParam<GlobalPosition>("Stokes.Grid.LowerLeft") * scaling;
    const auto freeFlowUpperRight = getParam<GlobalPosition>("Stokes.Grid.UpperRight") * scaling;

    auto channelElementSelector = [&](const auto& e)
    {
        const auto center = e.geometry().center();
        return (center[2] > freeFlowLowerLeft[2] && center[2] < freeFlowUpperRight[2] &&
                center[0] > freeFlowLowerLeft[0] && center[0] < freeFlowUpperRight[0] &&
                center[1] > freeFlowLowerLeft[1] && center[1] < freeFlowUpperRight[1]);
    };

    const bool singlePore = getParam<bool>("Grid.SinglePore", false);
    using HostGrid = Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double, 3>>;
    using SinglePoreGridCreator = GridManager<Dune::SubGrid<3, HostGrid>>;
    typename SinglePoreGridCreator::Grid* gridPtr;
    SinglePoreGridCreator singlePoreGridCreator;
    NetworkExtrusionGridCreator<3> networkExtrusionGridCreator;

    std::unique_ptr<HostGrid> hostGridPtr;

    if (singlePore)
    {
        const auto hostLowerLeft = getParam<GlobalPosition>("Grid.LowerLeft") * scaling;
        const auto hostUpperRight = getParam<GlobalPosition>("Grid.UpperRight") * scaling;
        auto hostCells = getParam<std::array<int, 3>>("Grid.Cells");

        static const auto refinement = getParam<int>("Grid.Refinement", 0);

        for (int i = 0; i < refinement; ++i)
            for (int& c : hostCells)
                c *= 2;

        hostGridPtr = std::make_unique<HostGrid>(hostLowerLeft, hostUpperRight, hostCells, std::bitset<3>{}, 1);

        // only consider a single pore at the interface
        const auto interfaceCenter = GlobalPosition{0.5*(freeFlowLowerLeft[0] + freeFlowUpperRight[0]),
                                                    0.5*(freeFlowLowerLeft[1] + freeFlowUpperRight[1]),
                                                    freeFlowLowerLeft[2]};
        std::cout << "found center of interface at " << interfaceCenter << std::endl;

        const double poreRadius = getParam<double>("PNM.Grid.PoreRadius") * scaling;
        std::cout << "pore radius is " << poreRadius << std::endl;

        auto elementSelector = [&](const auto& e)
        {
            return channelElementSelector(e) ||
                   (((e.geometry().center() - interfaceCenter).two_norm()) < poreRadius);
        };

        singlePoreGridCreator.init(*hostGridPtr, elementSelector);
        gridPtr = &singlePoreGridCreator.grid();
    }
    else
    {
        // extrude grid
        networkExtrusionGridCreator.init(lowDimGridManager, channelElementSelector);
        gridPtr = &networkExtrusionGridCreator.grid();
    }

    std::cout << "Creating grid took " << timer.elapsed() << " seconds." << std::endl;

    const auto& gridView = gridPtr->leafGridView();

#if ONLYCREATEGRID
    Dune::VTKWriter<std::decay_t<decltype(gridView)>> vtkWriter(gridView);
    vtkWriter.write("extruded_grid");
    Dune::VTKWriter<std::decay_t<decltype(lowDimGridManager.grid().leafGridView())>> vtkWriter1d(lowDimGridManager.grid().leafGridView());
    vtkWriter1d.write("1d_grid");
    Dune::GmshWriter<std::decay_t<decltype(gridView)>> gmshWriter(gridView);
    gmshWriter.setPrecision(10);
    gmshWriter.write("extruded_grid.msh");
#else

    // Define the sub problem type tags
    using TypeTag = Properties::TTag::StokesTypeTag;

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(gridView);
    gridGeometry->update();

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector sol;
    sol[GridGeometry::cellCenterIdx()].resize(gridGeometry->numCellCenterDofs());
    sol[GridGeometry::faceIdx()].resize(gridGeometry->numFaceDofs());

    // apply initial solution for instationary problems
    problem->applyInitialSolution(sol);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(sol);

    FluxOverSurface<GridVariables,
                    decltype(sol),
                    GetPropType<TypeTag, Properties::ModelTraits>,
                    GetPropType<TypeTag, Properties::LocalResidual>> flux(*gridVariables, sol);

    auto surfaces = getCouplingPoreSurfaces(lowDimGridManager, *gridGeometry, freeFlowLowerLeft, scaling);

    for (int i = 0; i < surfaces.size(); ++i)
    {
        std::cout << "adding surface " << i << " at " << surfaces[i].lowDimPos << std::endl;
        flux.addSurface(std::to_string(i), surfaces[i].corners[0], surfaces[i].corners[1], surfaces[i].corners[2], surfaces[i].corners[3]);
    }

    const int numAdditionalSurfaces = getParam<int>("FluxOverSurface.NumAdditionalSurfaces", 0);
    std::vector<std::string> surfaceNames;
    if (numAdditionalSurfaces > 0)
    {
        surfaceNames.reserve(numAdditionalSurfaces);
        for (int i = 0; i < numAdditionalSurfaces; ++i)
        {
            surfaceNames.emplace_back(getParam<std::string>("FluxOverSurface.Surface" + std::to_string(i) + ".Name"));
            const auto corner0 = getParam<GlobalPosition>("FluxOverSurface.Surface" + std::to_string(i) + ".LowerLeft");
            const auto corner1 = getParam<GlobalPosition>("FluxOverSurface.Surface" + std::to_string(i) + ".UpperRight");
            Dune::AxisAlignedCubeGeometry<double, GridGeometry::GridView::dimension-1, GridGeometry::GridView::dimensionworld> tmp(corner0, corner1);
            flux.addSurface(surfaceNames[i], tmp.corner(0), tmp.corner(1), tmp.corner(2), tmp.corner(3));
        }
    }

    StaggeredVtkOutputModule<GridVariables, decltype(sol)> vtkWriter(*gridVariables, sol, problem->name());
    GetPropType<TypeTag, Properties::IOFields>::initOutputModule(vtkWriter);

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables);

    // the linear solver
#if USEUMFPACK
    using LinearSolver = Dumux::UMFPackBackend;
#else
    using LinearSolver = Dumux::FreeFlowUzawaRestartedFlexibleGMResBackend<LinearSolverTraits<GridGeometry>>;
#endif
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    nonLinearSolver.solve(sol);

    std::cout << "numFaceDofs: " << gridGeometry->numFaceDofs()
              << ", numCellCenterDofs: " << gridGeometry->numCellCenterDofs() << ", total FF: " << gridGeometry->numFaceDofs() + gridGeometry->numCellCenterDofs() << std::endl;

    flux.calculateMassOrMoleFluxes();

    for (int i = 0; i < surfaces.size(); ++i)
        std::cout << "mass flux at pore" << i << " is: " << flux.netFlux(std::to_string(i)) << std::endl;

    for (const auto& name : surfaceNames)
        std::cout << "mass flux at surface " << name << " is: " << flux.netFlux(name) << std::endl;

    vtkWriter.write(1.0);

    const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    std::cout << "Simulation took " << timer.elapsed() << " seconds on "
              << comm.size() << " processes.\n"
              << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";

    // get gradients etc.
    using BoundaryTypes = typename ProblemTraits<Problem>::BoundaryTypes;
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
    using VelocityGradients = StaggeredVelocityGradients<double, GridGeometry, BoundaryTypes, Indices>;
    const auto data = EvalBeta<VelocityGradients>::eval(*gridVariables, sol);

    // create temporary stringstream with fixed formatting without affecting std::cout
    std::ostream tmp(std::cout.rdbuf());
    tmp << std::fixed << std::setprecision(15);

    tmp << "avg vx " << data.avgVx<< std::endl;
    tmp << "avg sumgrad " << data.avgSumGrad << std::endl;
    tmp << "avg beta" << data.avgBeta << std::endl;
    tmp << "avgSumGrad / avgVx " << data.avgSumGrad / data.avgVx << std::endl;

    if (singlePore)
    {
        const double poreRadius = getParam<double>("PNM.Grid.PoreRadius") * scaling;
        tmp << "beta scaled by radius " << data.avgSumGrad / data.avgVx * poreRadius << std::endl;
    }

#endif
    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
