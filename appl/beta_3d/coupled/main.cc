// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the 1d-3d embedded mixed-dimension model coupling two
 *        one-phase porous medium flow problems
 */

#ifndef ENABLESUBGRID
#define ENABLESUBGRID 1
#endif

#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/linear/ffpnmseqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>

#include <dumux/porenetworkflow/common/pnmvtkoutputmodule.hh>

#include <dumux/mixeddimension/boundary/pnmstokes/couplingmanager.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/porenetworkgridcreator.hh>
#include <dumux/linear/linearsolvertraits.hh>

#include "pnmproblem.hh"
#include "stokesproblem.hh"
#include "./../util.hh"

namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::StokesTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::PNMOnePTypeTag>;
    using type = Dumux::PNMStokesCouplingManager<Traits>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PNMOnePTypeTag>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::StokesTypeTag, Properties::TTag::StokesTypeTag, TypeTag>;
    using type = Dumux::PNMStokesCouplingManager<Traits>;
};


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesTypeTag>
{
private:
    static constexpr auto dim = 3;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<Scalar, 3>>;
};

} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    Dune::Timer timer;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    auto defaultParams = [](Dune::ParameterTree& tree) { };

    // parse command line arguments and input file
    Parameters::init(argc, argv, defaultParams, "");

    // Define the sub problem type tags
    using BulkTypeTag = Properties::TTag::StokesTypeTag;
    using LowDimTypeTag = Properties::TTag::PNMOnePTypeTag;

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using LowDimGridManager = Dumux::PoreNetworkGridCreator<3>;
    LowDimGridManager lowDimGridManager;
    lowDimGridManager.init("PNM"); // pass parameter group

    if (getParam<bool>("PNM.Grid.DeleteExcessCouplingThroats", true))
        deleteExcessCouplingThroats(lowDimGridManager.grid());

    // we compute on the leaf grid view
    const auto& lowDimGridView = lowDimGridManager.grid().leafGridView();


    GridManager<GetPropType<BulkTypeTag, Properties::Grid>> bulkGridManager;
    bulkGridManager.init("Stokes");


    const auto& bulkGridView = bulkGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using BulkGridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    auto bulkGridGeometry = std::make_shared<BulkGridGeometry>(bulkGridView);
    bulkGridGeometry->update();
    using LowDimGridGeometry = GetPropType<LowDimTypeTag, Properties::GridGeometry>;
    auto lowDimGridGeometry = std::make_shared<LowDimGridGeometry>(lowDimGridView);
    lowDimGridGeometry->update(*lowDimGridManager.getGridData());

    // the mixed dimension type traits
    using Traits = StaggeredMultiDomainTraits<BulkTypeTag, BulkTypeTag, LowDimTypeTag>;

    // the coupling manager
    using CouplingManager = PNMStokesCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the indices
    constexpr auto bulkCellCenterIdx = CouplingManager::bulkCellCenterIdx;
    constexpr auto bulkFaceIdx = CouplingManager::bulkFaceIdx;
    constexpr auto lowDimIdx = CouplingManager::lowDimIdx;

    // the problem (initial and boundary conditions)
    using BulkProblem = GetPropType<BulkTypeTag, Properties::Problem>;
    auto bulkProblem = std::make_shared<BulkProblem>(bulkGridGeometry, couplingManager);

    // the spatial parameters
    using LowDimSpatialParams = GetPropType<LowDimTypeTag, Properties::SpatialParams>;
    auto lowDimspatialParams = std::make_shared<LowDimSpatialParams>(lowDimGridGeometry);

    using LowDimProblem = GetPropType<LowDimTypeTag, Properties::Problem>;
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimGridGeometry, lowDimspatialParams, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    sol[bulkCellCenterIdx].resize(bulkGridGeometry->numCellCenterDofs());
    sol[bulkFaceIdx].resize(bulkGridGeometry->numFaceDofs());
    sol[lowDimIdx].resize(lowDimGridGeometry->numDofs());

    auto bulkSol = partial(sol, bulkFaceIdx, bulkCellCenterIdx);

    // apply initial solution for instationary problems
    bulkProblem->applyInitialSolution(bulkSol);
    lowDimProblem->applyInitialSolution(sol[lowDimIdx]);

    couplingManager->init(bulkProblem, lowDimProblem, sol);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkGridGeometry);
    bulkGridVariables->init(bulkSol);
    using LowDimGridVariables = GetPropType<LowDimTypeTag, Properties::GridVariables>;
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimGridGeometry);
    lowDimGridVariables->init(sol[lowDimIdx]);

    // pass the grid variables to the coupling manager
    couplingManager->setGridVariables(std::make_tuple(bulkGridVariables->faceGridVariablesPtr(),
                                                      bulkGridVariables->cellCenterGridVariablesPtr(),
                                                      lowDimGridVariables));

    // cache the beta slip factors for the throats at the interface
    bulkProblem->calculateBetas();

    // intialize the vtk output module
    const auto bulkName = getParam<std::string>("Problem.Name") + "_" + bulkProblem->name();
    const auto lowDimName = getParam<std::string>("Problem.Name") + "_" + lowDimProblem->name();

    std::vector<int> bulkElemCouplesToLowDimElem(bulkGridGeometry->gridView().size(0), -1);
    for (int i = 0; i < bulkElemCouplesToLowDimElem.size(); ++i)
    {
        if (couplingManager->bulkElementToLowDimElementMap().count(i))
            bulkElemCouplesToLowDimElem[i] = couplingManager->bulkElementToLowDimElementMap().at(i);
    }

    StaggeredVtkOutputModule<BulkGridVariables, decltype(bulkSol)> bulkVtkWriter(*bulkGridVariables, bulkSol, bulkName);
    bulkVtkWriter.addField(bulkElemCouplesToLowDimElem, "coupledLowDimElement", StaggeredVtkOutputModule<BulkGridVariables, decltype(bulkSol)>::FieldType::element);
    GetPropType<BulkTypeTag, Properties::IOFields>::initOutputModule(bulkVtkWriter);

    PNMVtkOutputModule<LowDimTypeTag> lowDimVtkWriter(*lowDimGridVariables, sol[lowDimIdx], lowDimName);
    GetPropType<LowDimTypeTag, Properties::IOFields>::initOutputModule(lowDimVtkWriter);
    // lowDimVtkWriter.write(0.0);


    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(bulkProblem, bulkProblem, lowDimProblem),
                                                 std::make_tuple(bulkGridGeometry->faceFVGridGeometryPtr(),
                                                                 bulkGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 lowDimGridGeometry),
                                                 std::make_tuple(bulkGridVariables->faceGridVariablesPtr(),
                                                                 bulkGridVariables->cellCenterGridVariablesPtr(),
                                                                 lowDimGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = Dumux::FreeFlowPNMUzawaGMRESBackend<LinearSolverTraits<BulkGridGeometry>>;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    Dune::Timer assembleAndSolveTimer;

    nonLinearSolver.solve(sol);

    std::cout << "Assembly and solve took " << assembleAndSolveTimer.elapsed() << " seconds" << std::endl;
     std::cout << "numFaceDofs: " << bulkGridGeometry->numFaceDofs()
               << ", numCellCenterDofs: " << bulkGridGeometry->numCellCenterDofs() << ", total FF: " << bulkGridGeometry->numFaceDofs() + bulkGridGeometry->numCellCenterDofs()
               << ", numPNMDofs: " << lowDimGridGeometry->numDofs()
               << ", total: " << bulkGridGeometry->numFaceDofs() + bulkGridGeometry->numCellCenterDofs() + lowDimGridGeometry->numDofs() << std::endl;

    bulkVtkWriter.write(1.0);
    lowDimVtkWriter.write(1.0);

    const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    std::cout << "Simulation took " << timer.elapsed() << " seconds on "
              << comm.size() << " processes.\n"
              << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
