// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model
 */
#ifndef DUMUX_CHANNEL_TEST_PROBLEM_HH
#define DUMUX_CHANNEL_TEST_PROBLEM_HH

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

namespace Dumux
{
template <class TypeTag>
class ChannelTestProblem;

namespace Properties
{
#if !NONISOTHERMAL
// Create new type tags
namespace TTag {
struct StokesTypeTag { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
} // end namespace TTag
#else
struct StokesTypeTag { using InheritsFrom = std::tuple<NavierStokesNI, StaggeredFreeFlowModel>; };
} // end namespace TTag
#endif

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StokesTypeTag>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
#if NONISOTHERMAL
    using type = FluidSystems::OnePLiquid<Scalar, SimpleH2O<Scalar> >;
#else
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
#endif
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StokesTypeTag> { using type = Dumux::ChannelTestProblem<TypeTag> ; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::StokesTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::StokesTypeTag> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::StokesTypeTag> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFaceVariablesCache<TypeTag, TTag::StokesTypeTag> { static constexpr bool value = false; };
}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the one-phase (Navier-) Stokes problem in a channel.
 * \todo doc me!
 */
template <class TypeTag>
class ChannelTestProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;

    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    ChannelTestProblem(std::shared_ptr<const GridGeometry> gridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry,  "Stokes"), eps_(1e-6), couplingManager_(couplingManager)
    {
        inletPressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InletPressure", 0.0);
        useSlipCondition_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseSlipCondition", false);
        fixedBeta_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.FixedBeta", 0.0);
        useShear_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseShear", false);
    }

    void calculateBetas()
    {
        if (useSlipCondition_)
        {
            if (std::abs(fixedBeta_) > 0.0)
                std::cout << "\nUsing fixed beta of " <<  fixedBeta_ << std::endl;

            auto fvGeometry = localView(this->gridGeometry());
            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                fvGeometry.bind(element);
                for (const auto& scvf : scvfs(fvGeometry))
                {
                    if (couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
                    {
                        if (std::abs(fixedBeta_) > 0.0)
                            beta_[scvf.index()] = fixedBeta_;
                        else
                        {
                            using std::pow;
                            const Scalar r = couplingManager().couplingData().coupledRadius(element, scvf);
                            beta_[scvf.index()] = 4.531 * pow(2*r, -1.067);
                        }
                    }
                }
            }
        }
    }

   /*!
     * \name Problem parameters
     */
    // \{


    bool shouldWriteRestartFile() const
    {
        return false;
    }

   /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    {
        return NumEqVector(0.0);
    }
    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos = scvf.dofPosition();

        if (isInlet(globalPos) || isOutlet(globalPos))
            values.setDirichlet(Indices::pressureIdx);
        else if (couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
        {
            values.setCouplingNeumann(Indices::momentumZBalanceIdx);
            values.setCouplingNeumann(Indices::conti0EqIdx);

            if (useSlipCondition_)
            {
                values.setBeaversJoseph(Indices::velocityXIdx);
                values.setBeaversJoseph(Indices::velocityYIdx);
            }
            else
            {
                values.setCouplingDirichlet(Indices::velocityXIdx);
                values.setCouplingDirichlet(Indices::velocityYIdx);
            }
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setDirichlet(Indices::velocityZIdx);

            if (useShear_)
            {
                if ((globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_) || (globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_))
                    values.setAllSymmetry();
            }
        }
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param element The element
     * \param scvf The subcontrolvolume face
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        const auto& globalPos = scvf.dofPosition();
        PrimaryVariables values;
        values[Indices::pressureIdx] = 0.0;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = 0.0;
        values[Indices::velocityZIdx] = 0.0;

        if (isInlet(globalPos))
            values[Indices::pressureIdx] = inletPressure_;

        if (isOutlet(globalPos))
        {
            static const Scalar outletPressure = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.OutletPressure", 0);
            values[Indices::pressureIdx] = outletPressure;
        }

        if (couplingManager().isCoupledEntity(CouplingManager::bulkFaceIdx, scvf) && !isInlet(globalPos))
        {
            const auto couplingVelocity = couplingManager().couplingData().boundaryVelocity(element, scvf, 1.0 /*extrusionFactor*/);
            values[Indices::velocityXIdx] = couplingVelocity[0];
            values[Indices::velocityYIdx] = couplingVelocity[1];
        }

        if (useShear_ && globalPos[2] > this->gridGeometry().bBoxMax()[2] - eps_)
        {
            static const Scalar shearVelocity = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.ShearVelocity");
            values[Indices::velocityXIdx] = shearVelocity;
        }

        return values;
    }

    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        if (couplingManager().isCoupledEntity(CouplingManager::bulkIdx, scvf))
        {
            values[Indices::conti0EqIdx] = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
            values[scvf.directionIndex()] = couplingManager().couplingData().momentumCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
        }
        return values;
    }

    /*!
     * \brief Returns the velocity in the porous medium (which is 0 by default according to Saffmann).
     */
    GlobalPosition porousMediumVelocity(const Element& element, const SubControlVolumeFace& scvf) const
    {
        if (couplingManager().isCoupledEntity(CouplingManager::bulkFaceIdx, scvf))
            return couplingManager().couplingData().boundaryVelocity(element, scvf, 1.0 /*boundaryVelocity*/);
        else
            return GlobalPosition(0.0);
    }

    /*!
     * \brief Returns the alpha value required as input parameter for the
              Beavers-Joseph-Saffman boundary condition.
     */
    Scalar betaBJ(const Element& element, const SubControlVolumeFace& scvf, const GlobalPosition& orientation) const
    { return beta_.at(scvf.index()); }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param element The element
     * \param scvf The subcontrolvolume face
     */
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    { return PrimaryVariables(0.0); }

    // \}

    template<class FluxOverPlane>
    void setPlanes(FluxOverPlane& flux, const std::vector<Scalar>& auxiliaryPositions)
    {
        const Scalar xMax = this->gridGeometry().bBoxMax()[0];
        const Scalar yMin = this->gridGeometry().bBoxMin()[1];
        const Scalar yMax = this->gridGeometry().bBoxMax()[1];

        //set a plane at the outlet
        const GlobalPosition pBottom{xMax, yMin};
        const GlobalPosition pTop{xMax, yMax};
        flux.addSurface("outlet", pBottom, pTop);

        const Scalar horizontalPlaneY = getParam<Scalar>("FluxOverSurface.HorizontalPlaneY");

        for(auto i : auxiliaryPositions)
            std::cout << i << std::endl;

        for(int i = 0; i < auxiliaryPositions.size(); i+=2)
        {
            const GlobalPosition pLeft{auxiliaryPositions[i] , horizontalPlaneY};
            const GlobalPosition pRight{auxiliaryPositions[i+1] , horizontalPlaneY};
            flux.addSurface("throatsVerticalFlux", pLeft, pRight);
        }
    }

    template<class FluxOverPlane>
    void printFluxes(const FluxOverPlane& flux) const
    {
        std::cout << "mass flux at outlet is: " << flux.netFlux("outlet") << std::endl;

        const auto& values = flux.values("throatsVerticalFlux");

        for(int i = 0; i < values.size(); ++i)
            std::cout << "throat " << i << " : " << values[i] << std::endl;

        std::cout << "\nvertical net flux (should be zero): "  << flux.netFlux("throatsVerticalFlux") << std::endl;
        std::cout << "\n##################################\n" << std::endl;
    }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return PrimaryVariables(0.0); }

private:

    bool isInlet(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_;
    }

    bool isOutlet(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_;
    }


    Scalar eps_;
    Scalar inletPressure_;
    bool useSlipCondition_;
    std::unordered_map<std::size_t, Scalar> beta_;
    Scalar fixedBeta_ = 0.0;
    bool useShear_ = false;


    std::shared_ptr<CouplingManager> couplingManager_;
};
} //end namespace

#endif
