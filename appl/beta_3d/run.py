import os
from contextlib import contextmanager
import subprocess
import numpy as np

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def run(model, polarAngle, azimuthAngle, pInlet, pBottom, beta, closedBottom):
    if closedBottom:
        caseName = str(model) + '_pol_' + str(polarAngle) + '_az_' + str(azimuthAngle) + '_pIn_' + str(pInlet) + '_closedBottom'
    else:
        caseName = str(model) + '_pol_' + str(polarAngle) + '_az_' + str(azimuthAngle) + '_pIn_' + str(pInlet) + '_pBot_' + str(pBottom)

    problemName = caseName

    with cd(os.path.abspath('reference' if model is 'ref' else 'coupled')):
        if os.path.isdir(problemName):
            print(problemName, 'already exists, overwriting files')
        else:
            os.system('mkdir ' + problemName)
        wd = os.getcwd()

        polarAngle = np.deg2rad(polarAngle)
        azimuthAngle =  np.deg2rad(azimuthAngle)

        z = 0.4e-3
        r = z / np.cos(polarAngle)
        x = r * np.sin(polarAngle) * np.cos(azimuthAngle)
        y = r * np.sin(polarAngle) * np.sin(azimuthAngle)

        pnmLowerLeft = [x, y ,z]

        print(pnmLowerLeft)
        print(r)

        command = './../beta_3d_reference ' if model is 'ref' else './../beta_3d_coupled '
        command += './../../params.input '
        command += '-Grid.Refinement ' + ('2 ' if model is 'ref' else '1 ')
        command += '-PNM.Grid.LowerLeft "' + str(-x) + ' ' + str(y) + ' '  + str(-z) + '" '
        command += '-Problem.Name ' + problemName + ' '
        command += '-Problem.InletPressure ' + str(pInlet) + ' '
        command += '-Problem.ClosedBottom ' + ('true ' if closedBottom else 'false ')
        command += '-Problem.UseSlipCondition ' + ('false ' if 'noslip' in problemName  else 'true ')
        command += '-Problem.BottomPressure ' + str(pBottom) + ' '
        command += '-Problem.FixedBeta ' + str(beta) + ' '
        command += ' > ' + problemName + '.log' + ' 2> ' + problemName + '.err '

        print(command)

        subprocess.call(command, cwd=wd + '/' + problemName, shell=True)


if __name__ == "__main__":
    idx = os.environ['SLURM_ARRAY_TASK_ID']
    idxToRun = [{'model':'ref'  ,         'polarAngle':0.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':True},
                {'model':'coupl_slip' ,   'polarAngle':0.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':True},
                {'model':'coupl_noslip' , 'polarAngle':0.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':True},
                {'model':'ref'  ,         'polarAngle':-45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':True},
                {'model':'coupl_slip' ,   'polarAngle':-45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':True},
                {'model':'coupl_noslip' , 'polarAngle':-45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':True},
                {'model':'ref'  ,         'polarAngle':0.0, 'azimuthAngle':0.0, 'pInlet':0e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_slip' ,   'polarAngle':0.0, 'azimuthAngle':0.0, 'pInlet':0e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_noslip' , 'polarAngle':0.0, 'azimuthAngle':0.0, 'pInlet':0e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'ref'  ,         'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':0e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_slip' ,   'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':0e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_noslip' , 'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':0e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'ref'  ,         'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_slip' ,   'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_noslip' , 'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-6, 'beta':57348.34, 'closedBottom':False},
                {'model':'ref'  ,         'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-3, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_slip' ,   'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-3, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_noslip' , 'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-3, 'beta':57348.34, 'closedBottom':False},
                {'model':'ref'  ,         'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-4, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_slip' ,   'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-4, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_noslip' , 'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':1e-4, 'beta':57348.34, 'closedBottom':False},
                {'model':'ref'  ,         'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_slip' ,   'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_noslip' , 'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':0, 'beta':57348.34, 'closedBottom':False},
                {'model':'ref'  ,         'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':-1e-5, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_slip' ,   'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':-1e-5, 'beta':57348.34, 'closedBottom':False},
                {'model':'coupl_noslip' , 'polarAngle':45.0, 'azimuthAngle':0.0, 'pInlet':1e-6, 'pBottom':-1e-5, 'beta':57348.34, 'closedBottom':False}]
    print(idx)
    idx = int(idx)
    run(model=idxToRun[idx]['model'],
        polarAngle=idxToRun[idx]['polarAngle'],
        azimuthAngle=idxToRun[idx]['azimuthAngle'],
        pInlet=idxToRun[idx]['pInlet'],
        pBottom=idxToRun[idx]['pBottom'],
        beta=idxToRun[idx]['beta'],
        closedBottom=idxToRun[idx]['closedBottom'])
