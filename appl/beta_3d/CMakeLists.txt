add_subdirectory(coupled)
add_subdirectory(reference)
add_input_file_links()
dune_symlink_to_source_files(FILES run.py startJob.sh)

install(FILES
        evalbeta.hh
        util.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/./appl/micromodel/beta_3d)
