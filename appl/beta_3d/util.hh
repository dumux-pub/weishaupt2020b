// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 */
#ifndef DUMUX_UTIL
#define DUMUX_UTIL

#include <vector>
#include <memory>
#include <iostream>
#include <random>

#include <dumux/common/parameters.hh>

namespace Dumux {

template<class Grid>
void deleteExcessCouplingThroats(Grid& grid)
{
    const auto& gridView = grid.leafGridView();
    const auto eps = 1e-8;

    std::unordered_map<std::size_t, std::vector<std::size_t>> elementsAtCouplingPores;
    using GlobalPosition = typename Grid::LeafGridView::template Codim<0>::Entity::Geometry::GlobalCoordinate;
    const auto couplingPosZ = getParam<GlobalPosition>("PNM.Grid.UpperRight")[2];

    for (const auto& element : elements(gridView))
    {
        for (int i = 0; i < 2; ++i)
        {
            auto vertex = element.template subEntity<1>(i);
            if (vertex.geometry().center()[2] > couplingPosZ - eps)
            {
                const auto vIdx = gridView.indexSet().index(vertex);
                const auto eIdx = gridView.indexSet().index(element);
                elementsAtCouplingPores[vIdx].push_back(eIdx);
            }
        }
    }

    std::mt19937 generator;
    const auto seed = getParam<std::size_t>("PNM.Grid.InterfaceDeletionRandomNumberSeed", 0);
    generator.seed(seed);

    std::vector<std::size_t> lowDimElementsToDelete;

    for (auto& values : elementsAtCouplingPores)
    {
        auto& eIndices = values.second;
        // remove duplicates
        std::sort(eIndices.begin(), eIndices.end());
        eIndices.erase(std::unique(eIndices.begin(), eIndices.end()), eIndices.end());

        std::shuffle(eIndices.begin(), eIndices.end(), generator);
        std::copy(eIndices.begin()+1, eIndices.end(), std::back_inserter(lowDimElementsToDelete));
    }


    grid.preGrow();
    for (const auto& element : elements(gridView))
    {
        const auto eIdx = gridView.indexSet().index(element);
        if (std::any_of(lowDimElementsToDelete.begin(), lowDimElementsToDelete.end(),
                       [&]( const int i ){ return i == eIdx; }))
        {
            std::cout << "removing element " << eIdx << std::endl;
            grid.removeElement(element);
        }
    }
    // triggers the grid growth process
    grid.grow();
    grid.postGrow();

    std::cout << "removal done "  << std::endl;
}

}

#endif // DUMUX_EVAL_BETA
