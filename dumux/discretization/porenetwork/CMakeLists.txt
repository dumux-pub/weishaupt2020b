
install(FILES
        fvelementgeometry.hh
        gridgeometry.hh
        subcontrolvolumeface.hh
        subcontrolvolume.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/discretization/porenetwork)
