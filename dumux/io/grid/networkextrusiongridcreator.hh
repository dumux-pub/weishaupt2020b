/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Grid generator that creates a 3D (or 2D) grid by extruding a 1D network grid.
 */

#include <vector>
#include <string>
#include <limits>
#include <dune/common/fvector.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

namespace Dumux {

template<int dim, class HostGrid = Dune::YaspGrid<3,Dune::EquidistantOffsetCoordinates<double, dim>>>
class NetworkExtrusionGridCreator : public GridManager<Dune::SubGrid<dim, HostGrid>>
{
    using ParentType = GridManager<Dune::SubGrid<dim, HostGrid>>;
    using ctype = typename HostGrid::ctype;

    using Point = Dune::FieldVector<ctype, dim>;
    struct Segment { Point a, b; double radius; };
    struct Node { Point a; double radius; };

public:

    /*!
     * \brief Create a grid by extruding a 1D network
     *
     * \param networkGridManager The 1D grid manager
     * \param otherElementSelector Optionanl elementSelector to define the domain beyond the network
     * \param paramGroup Optional parameter group name
     */
    template<class NetworkGridManager, class OtherElementSelector>
    void init(NetworkGridManager& networkGridManager,
              const OtherElementSelector& otherElementSelector = [](const auto& e){ return false; },
              const std::string& paramGroup = "")
    {
        const auto& networkGridView = networkGridManager.grid().leafGridView();
        Point bBoxMin = Point(std::numeric_limits<ctype>::max());
        Point bBoxMax = Point(std::numeric_limits<ctype>::min());
        std::vector<Segment> segments(networkGridView.size(0));
        std::vector<Node> nodes(networkGridView.size(1));
        const auto& gridData = networkGridManager.getGridData();

        // extract the segments
        for (const auto& element : elements(networkGridView))
        {
            const auto geometry = element.geometry();
            const auto eIdx = networkGridView.indexSet().index(element);
            auto level0element = element;
            while (level0element.hasFather())
                level0element = level0element.father();

            static const auto throatRadiusIdx = gridData->parameterIndex("ThroatRadius");
            const auto throatRadius = gridData->parameters(level0element)[throatRadiusIdx];
            segments[eIdx] = Segment{geometry.corner(0), geometry.corner(1), throatRadius};
        }

        // extract the nodes and the bounding box of the network
        for (const auto& vertex : vertices(networkGridView))
        {
            const auto vIdx = networkGridView.indexSet().index(vertex);
            nodes[vIdx].a = vertex.geometry().center();

            static const auto poreRadiusIdx = gridData->parameterIndex("PoreRadius");
            const auto poreRadius = gridData->parameters(vertex)[poreRadiusIdx];
            nodes[vIdx].radius = poreRadius;

            for (int i = 0; i < Point::dimension; i++)
            {
                using std::min;
                using std::max;
                bBoxMin[i] = min(bBoxMin[i], nodes[vIdx].a[i] - poreRadius);
                bBoxMax[i] = max(bBoxMax[i], nodes[vIdx].a[i] + poreRadius);
            }
        }

        const ctype eps = (bBoxMin - bBoxMax).two_norm() * 1e-12;

        auto elementSelector = [&](const auto& element)
        {
            if (otherElementSelector(element))
                return true;

            const auto center = element.geometry().center();
            if (!pointIsWithinBoundingBox_(center, bBoxMin, bBoxMax, eps))
                return false;

            for (const auto& segment : segments)
                if (pointIsWithinCylinder_(center, segment))
                    return true;

            for (const auto& node : nodes)
                if (pointIsWithinSphere_(center, node))
                    return true;

            return false;
        };

        ParentType::init(elementSelector, paramGroup);
    };

private:

    // Return whether a point lies within a bounding box
    bool pointIsWithinBoundingBox_(const Point& p, const Point& bBoxMin, const Point& bBoxMax, ctype eps) const
    {
        return (p[0] < bBoxMax[0] + eps && p[0] > bBoxMin[0] - eps &&
                p[1] < bBoxMax[1] + eps && p[1] > bBoxMin[1] - eps &&
                p[2] < bBoxMax[2] + eps && p[2] > bBoxMin[2] - eps);
    }

    // Return whether a point lies within a cylinder described by the segment
    bool pointIsWithinCylinder_(const Point& p, const Segment& segment) const
    { return std::signbit(distanceToSegment_(p, segment.a, segment.b) - segment.radius); }

    // Return whether a point lies within a sphere described by the node
    bool pointIsWithinSphere_(const Point& p, const Node& node) const
    { return std::signbit(distanceToNode_(p, node.a) - node.radius); }

    // Compute the distance of p to the segment connecting a->b
    auto distanceToSegment_(const Point& p, const Point& a, const Point& b) const
    {
        const auto v = b - a;
        const auto w = p - a;

        const auto proj1 = v*w;
        if (proj1 <= 0.0)
            return w.two_norm();

        const auto proj2 = v.two_norm2();
        if (proj2 <= proj1)
            return (p - b).two_norm();

        const auto t = proj1 / proj2;
        auto x = a;
        x.axpy(t, v);
        return (x - p).two_norm();
    }

    // Compute the distance of p to the node a
    auto distanceToNode_(const Point& p, const Point& a) const
    {
        const auto dist = p - a;
        return dist.two_norm();
    }
};

} // end namespace Dumux
