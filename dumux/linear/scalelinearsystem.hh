// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Linear
 * \brief Scale the linear system
 */

 #ifndef DUMUX_SCALE_LINEAR_SYSTEM_HH
 #define DUMUX_SCALE_LINEAR_SYSTEM_HH

 #include <vector>
 #include <dune/common/indices.hh>

 /*!
 * \ingroup Linear
 * \brief Scale the linear system by the inverse of
 * its (block-)diagonal entries.
 *
 * \param matrix the matrix to scale
 * \param rhs the right hand side vector to scale
 */
template <class Matrix, class OffDiagMatrix, class Vector>
auto scaleLinearSystemByInverseOfDiagonal(Matrix& matrix, OffDiagMatrix& offDiagMatrix, Vector& rhs)
{
    auto tmp = rhs;

    for (auto row = matrix.begin(); row != matrix.end(); ++row)
    {
        const auto rowIdx = row.index();

        auto diagonal = matrix[rowIdx][rowIdx];
        diagonal.invert();

        const auto b = rhs[rowIdx];
        diagonal.mv(b, rhs[rowIdx]);

        tmp[rowIdx] = diagonal;

        for (auto col = row->begin(); col != row->end(); ++col)
            col->leftmultiply(diagonal);
    }

    for (auto offDiagrow = offDiagMatrix.begin(); offDiagrow != offDiagMatrix.end(); ++offDiagrow)
    {
        const auto rowIdx = offDiagrow.index();

        const auto& diagonal = tmp[rowIdx];
        // std::cout << "vblock diag " << rowIdx << " : " << 1/diagonal << std::endl;


        auto col = offDiagrow->begin();
        for (; col != offDiagrow->end(); ++col)
        {
            // std::cout << "old entry in p block: " << col.index() << ": " << *col << std::endl;
            *col *= diagonal;
        }
            // col->leftmultiply(diagonal);
    }

    return tmp;
}

 /*!
 * \ingroup Linear
 * \brief Scale the linear system by the inverse of
 * its (block-)diagonal entries.
 *
 * \param matrix the matrix to scale
 * \param rhs the right hand side vector to scale
 */
template <class Matrix, class OffDiagMatrix, class Vector>
auto scaleLinearSystemByInverseOfMaxValue(Matrix& matrix, OffDiagMatrix& offDiagMatrix, Vector& rhs)
{
    auto& rhs0 = rhs[Dune::Indices::_0];

    std::vector<double> maxValue(rhs0.size());

    // get max val
    for (auto row = matrix.begin(); row != matrix.end(); ++row)
    {
        for (auto col = row->begin(); col != row->end(); ++col)
        {
            if (std::abs(matrix[row.index()][col.index()] > std::abs(maxValue[row.index()])))
            {
                // std::cout << "in diag block " << row.index() << ", " << col.index() << ": " << matrix[row.index()][col.index()] << std::endl;
                maxValue[row.index()] = matrix[row.index()][col.index()];
            }
        }
    }
    for (auto row = offDiagMatrix.begin(); row != offDiagMatrix.end(); ++row)
    {
        for (auto col = row->begin(); col != row->end(); ++col)
        {
            if (std::abs(offDiagMatrix[row.index()][col.index()] > std::abs(maxValue[row.index()])))
            {
                // std::cout << "in off diag block " << row.index() << ", " << col.index() << ": " << offDiagMatrix[row.index()][col.index()] << std::endl;
                maxValue[row.index()] = offDiagMatrix[row.index()][col.index()];
            }
        }
    }



    // do scaling
    for (auto row = matrix.begin(); row != matrix.end(); ++row)
    {
        rhs0[row.index()] /= maxValue[row.index()];
        for (auto col = row->begin(); col != row->end(); ++col)
        {
            // std::cout << "max val " << maxValue[row.index()] << std::endl;
            matrix[row.index()][col.index()] /= maxValue[row.index()];
        }
    }

    for (auto row = offDiagMatrix.begin(); row != offDiagMatrix.end(); ++row)
    {
        for (auto col = row->begin(); col != row->end(); ++col)
            offDiagMatrix[row.index()][col.index()] /= maxValue[row.index()];
    }

}

template<class Matrix, class Vector>
void eliminateDirichletEntriesFromColumns(Matrix& matrix, Vector& rhs)
{

    // find Dirichlet values TODO improve
    std::vector<bool> isDirichlet(matrix.N(), false);
    for (int i = 0; i < matrix.N(); ++i)
    {
        if (std::abs(matrix[i][i].frobenius_norm() - 1.0) < 1e-14)
            isDirichlet[i] = true;
    }

    // std::cout << "found " << std::count_if(isDirichlet.begin(), isDirichlet.end(), [](bool i) {return i;}) << "velocity dirichlet entries" << std::endl;

    for (auto row = matrix.begin(); row != matrix.end(); ++row)
    {
        const auto rowIdx = row.index();
        if (isDirichlet[rowIdx])
            continue;

        for (auto col = row->begin(); col != row->end(); ++col)
        {
            const auto colIdx = col.index();
            if (rowIdx != colIdx && isDirichlet[colIdx])
            {
                // std::cout << "row " << rowIdx << ", col " << colIdx << ": ";
                auto& entryToDelete = matrix[rowIdx][colIdx];
                // std::cout << "entryToDelete " << entryToDelete << ", " ;
                auto tmp = rhs[Dune::Indices::_1][rowIdx];
                // std::cout << "dirichlet value " << tmp <<  std::endl;
                tmp *= entryToDelete;
                rhs[Dune::Indices::_1][rowIdx] -= tmp;
                entryToDelete = 0;
            }
        }
    }
}

#endif // DUMUX_SCALE_LINEAR_SYSTEM_HH
