// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Linear
 * \brief Dumux sequential linear solver backends
 */
#ifndef DUMUX_FF_PNM_SEQ_SOLVER_BACKEND_HH
#define DUMUX_FF_PNM_SEQ_SOLVER_BACKEND_HH

#include <type_traits>
#include <utility>

#include <dune/istl/preconditioners.hh>
#include <dune/common/indices.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/linear/solver.hh>
#include <dumux/linear/scalelinearsystem.hh>
#include <dumux/linear/seqsolverbackend.hh>

namespace Dumux {

namespace Detail {

//! This is a partial copy of Dune::MultiTypeBlockMatrix. TODO: It can be deleted once Dune::MultiTypeBlockMatrix
//! exposes std::tuple's constructor.
template<class FirstRow, class... Args>
class MultiTypeBlockMatrix : std::tuple<FirstRow, Args...>
{
    using ParentType = std::tuple<FirstRow, Args...>;
  public:

    using ParentType::ParentType;

    /**
     * own class' type
     */
    using type = MultiTypeBlockMatrix<FirstRow, Args...>;

    /** \brief Type used for sizes */
    using size_type = std::size_t;

    using field_type = typename FirstRow::field_type;

    /** \brief Return the number of matrix rows */
    static constexpr size_type N()
    {
      return 1+sizeof...(Args);
    }

    /** \brief Return the number of matrix columns */
    static constexpr size_type M()
    {
      return FirstRow::size();
    }

    template< size_type index >
    auto
    operator[] ( const std::integral_constant< size_type, index > indexVariable ) -> decltype(std::get<index>(*this))
    {
      DUNE_UNUSED_PARAMETER(indexVariable);
      return std::get<index>(*this);
    }

   /** \brief Const random-access operator
     *
     * This is the const version of the random-access operator.  See the non-const version for a full
     * explanation of how to use it.
     */
    template< size_type index >
    auto
    operator[] ( const std::integral_constant< size_type, index > indexVariable ) const -> decltype(std::get<index>(*this))
    {
      DUNE_UNUSED_PARAMETER(indexVariable);
      return std::get<index>(*this);
    }

    /** \brief y = A x
     */
    template<typename X, typename Y>
    void mv (const X& x, Y& y) const {
      static_assert(X::size() == M(), "length of x does not match row length");
      static_assert(Y::size() == N(), "length of y does not match row count");
      y = 0; //reset y (for mv uses umv)
      umv(x,y);
    }

    /** \brief y += A x
     */
    template<typename X, typename Y>
    void umv (const X& x, Y& y) const {
      static_assert(X::size() == M(), "length of x does not match row length");
      static_assert(Y::size() == N(), "length of y does not match row count");
      using namespace Dune::Hybrid;
      forEach(integralRange(Dune::Hybrid::size(y)), [&](auto&& i) {
        using namespace Dune::Hybrid; // needed for icc, see issue #31
        forEach(integralRange(Dune::Hybrid::size(x)), [&](auto&& j) {
          (*this)[i][j].umv(x[j], y[i]);
        });
      });
    }

    /** \brief y += alpha A x
     */
    template<typename AlphaType, typename X, typename Y>
    void usmv (const AlphaType& alpha, const X& x, Y& y) const {
      static_assert(X::size() == M(), "length of x does not match row length");
      static_assert(Y::size() == N(), "length of y does not match row count");
      using namespace Dune::Hybrid;
      forEach(integralRange(Dune::Hybrid::size(y)), [&](auto&& i) {
        using namespace Dune::Hybrid; // needed for icc, see issue #31
        forEach(integralRange(Dune::Hybrid::size(x)), [&](auto&& j) {
          (*this)[i][j].usmv(alpha, x[j], y[i]);
        });
      });
    }

};

/*!
 * \brief a function to get a MultiTypeBlockVector with const references to some entries of another MultiTypeBlockVector
 * \param v a MultiTypeBlockVector
 * \param indices the indices of the entries that should be referenced
 * TODO can be removed if it gets implemented in dumux-master
 */
template<class ...Args, std::size_t ...i>
auto partial(const Dune::MultiTypeBlockVector<Args...>& v, Dune::index_constant<i>... indices)
{
    return Dune::MultiTypeBlockVector<std::add_lvalue_reference_t<const std::decay_t<std::tuple_element_t<indices, std::tuple<Args...>>>>...>(v[indices]...);
}

template<class M, class X, class Y>
class FreeFlowBlockHelper
{
    using A00 = std::decay_t<decltype(std::declval<M>()[Dune::Indices::_0][Dune::Indices::_0])>;
    using A01 = std::decay_t<decltype(std::declval<M>()[Dune::Indices::_0][Dune::Indices::_1])>;
    using A10 = std::decay_t<decltype(std::declval<M>()[Dune::Indices::_1][Dune::Indices::_0])>;
    using A11 = std::decay_t<decltype(std::declval<M>()[Dune::Indices::_1][Dune::Indices::_1])>;
    using V0 = std::decay_t<decltype(std::declval<X>()[Dune::Indices::_0])>;
    using V1 = std::decay_t<decltype(std::declval<X>()[Dune::Indices::_1])>;
    using FirstRow = Dune::MultiTypeBlockVector<const A00&, const A01&>;
    using SecondRow = Dune::MultiTypeBlockVector<const A10&, const A11&>;
public:
    using Matrix = MultiTypeBlockMatrix<FirstRow, SecondRow>;
    using NewX = Dune::MultiTypeBlockVector<V0&, V1&>;
    using NewY = Dune::MultiTypeBlockVector<V0, V1>;

    using BaseUzawa = Dumux::SeqUzawa<Matrix, NewX, NewY>;
    using MatrixAdapter = Dune::MatrixAdapter<Matrix, NewX, NewY>;

    static Matrix getFreeFlowMatrix(const M& m)
    {
        using namespace Dune::Indices;  // for _0, _1, etc.
        const auto& a00 = m[_0][_0];
        const auto& a01 = m[_0][_1];
        const auto& a10 = m[_1][_0];
        const auto& a11 = m[_1][_1];

        FirstRow firstRow(a00, a01);
        SecondRow secondRow(a10, a11);

        return MultiTypeBlockMatrix<FirstRow, SecondRow>(firstRow, secondRow);
    }
};

}

template<class... Args>
struct isMultiTypeBlockMatrix<Detail::MultiTypeBlockMatrix<Args...>> : public std::true_type {};

template<class M, class X, class Y>
class SeqUzawaWithJacobi : public SeqUzawa<M, X, Y>
{
    using ParentType = SeqUzawa<M, X, Y>;
    using VelocityMatrix = typename std::remove_reference<decltype(std::declval<M>()[Dune::Indices::_0][Dune::Indices::_0])>::type;
    using VelocityVector = typename std::remove_reference<decltype(std::declval<X>()[Dune::Indices::_0])>::type;
    using SeqJac = Dune::SeqJac<VelocityMatrix, VelocityVector, VelocityVector>;
public:

    SeqUzawaWithJacobi(const std::shared_ptr<const Dune::AssembledLinearOperator<M,X,Y>>& op, const Dune::ParameterTree& params)
    : ParentType(op, params)
    , _A_(op->getmat())
    {
        using namespace Dune::Indices;  // for _0, _1, etc.
        seqJac_ = std::make_unique<SeqJac>(_A_[_0][_0], 1, 1.0);
    }

    /*!
     *   \brief Apply the preconditioner
     *
     *   \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d) final
    {
        using namespace Dune::Indices;
        static const bool applyJacobi = getParam<bool>("LinearSolver.ApplyJacobiPreconditioner", false);
        if (applyJacobi)
            seqJac_->apply(v[_0], d[_0]);

        ParentType::apply(v,d);
    }

private:
        std::unique_ptr<SeqJac> seqJac_;
        const M& _A_;
};


template<class M, class X, class Y>
class FreeFlowPNMSeqUzawa : public Dune::Preconditioner<X,Y>
{
    using Helper = Detail::FreeFlowBlockHelper<M,X,Y>;
    using BaseUzawa = typename Helper::BaseUzawa;

    using PNMMatrix = typename std::remove_reference<decltype(std::declval<M>()[Dune::Indices::_2][Dune::Indices::_2])>::type;
    using PNMVector = typename std::remove_reference<decltype(std::declval<X>()[Dune::Indices::_2])>::type;

    using SeqJac = Dune::SeqJac<PNMMatrix, PNMVector, PNMVector>;
    using FFBlock = typename Helper::Matrix;

public:

    /*! \brief Constructor.
     */
    FreeFlowPNMSeqUzawa(const std::shared_ptr<const Dune::AssembledLinearOperator<M,X,Y>>& op, const Dune::ParameterTree& params)
    : mat_(Helper::getFreeFlowMatrix(op->getmat()))
    {
        using namespace Dune::Indices;  // for _0, _1, etc.
        seqJac_ = std::make_unique<SeqJac>(op->getmat()[_2][_2], 1, 1.0);
        using MatrixAdapter = typename Helper::MatrixAdapter;
        const auto linearOperator = std::make_shared<MatrixAdapter>(mat_);
        baseUzawa_ = std::make_unique<BaseUzawa>(linearOperator, params);
    }

    /*!
     *   \brief Prepare the preconditioner.
     *
     *   \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b) {}

    /*!
     *   \brief Apply the preconditioner
     *
     *   \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d) final
    {
        using namespace Dune::Indices;
        auto vFF = partial(v, _0, _1);
        const auto dFF = Detail::partial(d, _0, _1);
        baseUzawa_->apply(vFF, dFF);
        seqJac_->apply(v[_2], d[_2]);
    }

    /*!
     *   \brief Clean up.
     *
     *   \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x) {}

    //! Category of the preconditioner (see SolverCategory::Category)
    virtual Dune::SolverCategory::Category category() const
    {
        return Dune::SolverCategory::sequential;
    }

private:

    std::unique_ptr<BaseUzawa> baseUzawa_;
    std::unique_ptr<SeqJac> seqJac_;
    const FFBlock mat_;
};

template <class LinearSolverTraits>
class FreeFlowPNMUzawaBiCGSTABBackend : public LinearSolver
{
public:
    using LinearSolver::LinearSolver;

    // precondBlockLevel: set this to more than one if the matrix to solve is nested multiple times
    template<int precondBlockLevel = 1, class Matrix, class Vector>
    bool solve(const Matrix& A, Vector& x, const Vector& b)
    {
        using Preconditioner = FreeFlowPNMSeqUzawa<Matrix, Vector, Vector>;
        using Solver = Dune::BiCGSTABSolver<Vector>;

        static const auto solverParams = LinearSolverParameters<LinearSolverTraits>::createParameterTree(this->paramGroup());
        return IterativePreconditionedSolverImpl::template solveWithParamTree<Preconditioner, Solver>(A, x, b, solverParams);
    }

    std::string name() const
    {
        return "Uzawa preconditioned BiCGSTAB solver";
    }
};

template <class LinearSolverTraits>
class FreeFlowPNMUzawaGMRESBackend : public LinearSolver
{
public:
    using LinearSolver::LinearSolver;

    // precondBlockLevel: set this to more than one if the matrix to solve is nested multiple times
    template<int precondBlockLevel = 1, class Matrix, class Vector>
    bool solve(const Matrix& A, Vector& x, const Vector& b)
    {
        using Preconditioner = FreeFlowPNMSeqUzawa<Matrix, Vector, Vector>;
        using Solver = Dune::RestartedFlexibleGMResSolver<Vector>;

        static const auto solverParams = LinearSolverParameters<LinearSolverTraits>::createParameterTree(this->paramGroup());
        return IterativePreconditionedSolverImpl::template solveWithParamTree<Preconditioner, Solver>(A, x, b, solverParams);
    }

    std::string name() const
    {
        return "Uzawa preconditioned RestartedGMRES solver";
    }
};

template <class LinearSolverTraits>
class FreeFlowUzawaBiCGSTABBackend : public LinearSolver
{
public:
    using LinearSolver::LinearSolver;

    // precondBlockLevel: set this to more than one if the matrix to solve is nested multiple times
    template<int precondBlockLevel = 1, class Matrix, class Vector>
    bool solve(Matrix& A, Vector& x, Vector& b)
    {
        using namespace Dune::Indices;

        using Preconditioner = SeqUzawaWithJacobi<Matrix, Vector, Vector>;
        using Solver = Dune::BiCGSTABSolver<Vector>;


        Vector tmp;
        if (getParam<bool>("LinearSolver.ApplyScaling"))
            tmp = scaleLinearSystemByInverseOfDiagonal(A[_1][_1], A[_1][_0], b[_1]);

        static const auto solverParams = LinearSolverParameters<LinearSolverTraits>::createParameterTree(this->paramGroup());
        return IterativePreconditionedSolverImpl::template solveWithParamTree<Preconditioner, Solver>(A, x, b, solverParams);
    }

    std::string name() const
    {
        return "Uzawa preconditioned BiCGSTAB solver";
    }
};

template <class LinearSolverTraits>
class FreeFlowUzawaRestartedFlexibleGMResBackend : public LinearSolver
{
public:
    using LinearSolver::LinearSolver;

    // precondBlockLevel: set this to more than one if the matrix to solve is nested multiple times
    template<int precondBlockLevel = 1, class Matrix, class Vector>
    bool solve(Matrix& A, Vector& x, Vector& b)
    {
        using Preconditioner = SeqUzawaWithJacobi<Matrix, Vector, Vector>;
        using Solver = Dune::RestartedFlexibleGMResSolver<Vector>;

        Vector tmp;
        if (getParam<bool>("LinearSolver.ApplyScaling"))
            tmp = scaleLinearSystemByInverseOfDiagonal(A[Dune::Indices::_1][Dune::Indices::_1], A[Dune::Indices::_1][Dune::Indices::_0], b[Dune::Indices::_1]);


        static const auto solverParams = LinearSolverParameters<LinearSolverTraits>::createParameterTree(this->paramGroup());
        return IterativePreconditionedSolverImpl::template solveWithParamTree<Preconditioner, Solver>(A, x, b, solverParams);
    }

    std::string name() const
    {
        return "Uzawa preconditioned RestartedFlexibleGMRes solver";
    }
};



// \}

} // end namespace Dumux

#endif
