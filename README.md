Summary
=======
This is the DuMuX module containing the code for producing the results
of the paper:

A hybrid-dimensional coupled pore-network/free-flow
model including pore-scale slip and its application to
a micromodel experiment



Installation
============
The easiest way to install this module is to create a new folder and to execute the file
[installweishaupt2020b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2020b/-/raw/master/installweishaupt2020b.sh)
in this folder.

```bash
mkdir -p Weishaupt2020b && cd Weishaupt2020b
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2020b/-/raw/master/installweishaupt2020b.sh && chmod +x installweishaupt2020b.sh
sh ./installweishaupt2020b.sh
```


## Dependencies on other DUNE modules

| module | branch | commit hash |
|:-------|:-------|:------------|
| dune-common | master | e73048ff0710a310ba4ccc81cf739d6dd501aa2f |
| dune-geometry | master | 073077786cf9ea6240653924af035ff07f12e391 |
| dune-grid | master | 5094279f10eb0aa999fe8e9982e6578819f57dba |
| dune-localfunctions | master | 0d1685fec57f3874ec94a8245f9e51f31a1a073e |
| dune-istl | master | ff799f4f4bcf774aa20a851909934b4e82d1d2a8 |
| dune-foamgrid | master | bd68a6ff42136621c5321acabea17ac10d4e3997 |
| dune-subgrid | master | 1bc0b375ffdcf39222416ecafea47db6f08e91f6 |
| dumux | master | 2b075424e4021400ecf443628e51d855dd10d650 |


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Weishaupt2020b
cd Weishaupt2020b
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/weishaupt2020b/-/raw/master/docker_weishaupt2020b.sh
```

Open the Docker Container
```bash
bash docker_weishaupt2020b.sh open
```

After the script has run successfully, you may build all executables

```bash
cd weishaupt2020b/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths:

- appl/micromodel_1p/coupled
- appl/micromodel_1p/reference
- appl/beta_3d/coupled
- appl/beta_3d/reference
- appl/beta_2d
